import React from "react";
import "./startCamp.css";
import "./infoPannel-style.css"
import { Breadcrumb, BreadcrumbItem, Row, Col, Tooltip } from "reactstrap"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
var time = new Date().getHours()
var step = "2"
class infoPannel extends React.Component {
    constructor() {
        super();
        this.state = {
            tooltipOpen: false
        }
    }
    toggle = () => {
        this.setState({
            tooltipOpen: !this.state.tooltipOpen
        })
    }
    render() {
        const { step, name, selectedMethod, selectedPackage, changeStep,ScheduleData,uploadData } = this.props
        return (
            <div className="toppanelContainer">


                <div className="greetingContainer">
                    <div className="icon" >
                        {time < 12 ?
                            <FontAwesomeIcon size="3x" icon={"sun"}></FontAwesomeIcon>
                            :
                            <FontAwesomeIcon size="3x" icon={"cloud-moon"}></FontAwesomeIcon>
                        }</div>

                    <div className="greetingText">
                        {
                            time < 12 ?
                                `Good Morning!,`
                                :
                                `Good Evening!,`

                        }
                        <br>
                        </br>
                        {name}
                    </div>
                    <div className="vl"></div>
                </div>
                
                <div className="info" >
                    Let's get start planning your campaign
                    <ol className="steplist" >
                        <li className={selectedMethod!==""?"text-success":"text-dark"}>Select advertising method
                            
                        </li>
                        <li className={selectedPackage.name!==""?"text-success":"text-dark"}>
                            Select package
                        </li>
                        <li className={uploadData.AdDesign!==""||uploadData.designFile?"text-success":"text-dark"}>
                            Upload advertisement
                        </li>
                        <li className={ScheduleData!==""?"text-success":"text-dark"}>
                            Schedule the campaign
                        </li>
                        <li>
                            Payment
                        </li>

                    </ol>
                </div>




            </div>






        )
    }
}


export default infoPannel;