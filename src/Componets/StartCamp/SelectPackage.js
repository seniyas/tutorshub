import React from "react";
import FbNewsFeed from "./FacebookNewsFeed"
import FacebookPage from "./FacebookPage"
import FacebookGroup from "./FacebookGroup"
import InAppAds from "./InAppAds"
class SelectPackage extends React.Component {
    constructor() {
        super();
    }

    render() {
        const { selectedMethod,ChangeSelectedPackage } = this.props
        return (
            <div>
                {selectedMethod === "1" ?
                    <FbNewsFeed ChangeSelectedPackage={ChangeSelectedPackage()}></FbNewsFeed>
                    :selectedMethod==="2"?
                    <FacebookGroup></FacebookGroup>
                    :selectedMethod==="3"?
                    <FacebookPage></FacebookPage>
                    :
                    <InAppAds></InAppAds>

                }

            </div>
        )
    }
}


export default SelectPackage;