import React from "react"
import { CustomInput, Form, Collapse, Fade, FormGroup, Label, Button, Row, Col, Input, FormFeedback } from 'reactstrap';

import "./startCamp.css";
class UploadAd extends React.Component {

    constructor() {
        super();
        this.state = {
            isDesign: false,
            textTODisplay: "",
            isContact: false,
            contactNum: "",
            date: "",
            time: "",
            AdUrl: "",
            designFileUrl: "",


            noDate: false,
            noContactNum: false,
            contactNumError: "",
            dateError: "",
            TimeError: ""
        };
        this.toggle = this.toggle.bind(this);
        this.ContactToggle = this.ContactToggle.bind(this)

    }

   
    Validate = () => {

        if (this.state.isContact === true) {

            //mobile number is not entered
            if (this.state.contactNum === "") {

                this.setState({
                    noContactNum: true,
                    contactNumError: "Please enter your mobile number to contact you"

                })
                return false;
            } else if (!this.state.contactNum.length === 10) {

                this.setState({ noContactNum: true, contactNumError: "Please enter a valid contact number" })
                return false;
            }
            if (this.state.time === "") {

                this.setState({ noTime: true })
                return false;
            }
            if (this.state.date == "") {

                this.setState({ noDate: true })
                return false;
            }


            if (!this.state.contactNum === "") {

                this.setState({
                    noContactNum: false,
                    contactNumError: ""

                })
                return true;
            } else if (!this.state.contactNum.length === 10) {

                this.setState({ noContactNum: false, contactNumError: "" })
                return true;
            }
            if (!this.state.time === "") {

                this.setState({ noTime: false })
                return true;
            }
            if (!this.state.date == "") {
                this.setState({ noDate: false })
                return true;
            }
        } else {
            return true;
        }
    }

    submit = () => {
        const {
            isDesign,
            textTODisplay,
            isContact,
            contactNum,
            date,
            time,
            AdUrl,
            designFileUrl, } = this.state
        const data = {
            isDesign: isDesign,
            textOnDesign: textTODisplay,
            isContact: isContact,
            contactNum: contactNum,
            contactDate: date,
            contactTime: time,
            AdDesign: AdUrl,
            designFile: designFileUrl,
        }


        this.props.ChangeCampData(data)
    }
    checkTelNumber = (value) => {
        var phoneNo = /^\d{10}$/;
        value === "" || !value.match(phoneNo) ?
            this.setState({ noContactNum: true, contactNumError: "Please enter a valid contact number" })
            :
            this.setState({ noContactNum: false, contactNumError: "" })
        this.setState({ contactNum: value })
    }
    checkDate = (value) => {
        let nowDate = new Date();

        let checkDate = new Date(value);

        if (nowDate < checkDate) {
            this.setState({ dateError: "" })
        } else {
            if (nowDate.getDate() === checkDate.getDate() && nowDate.getMonth() === checkDate.getMonth() && nowDate.getFullYear() === checkDate.getFullYear()) {
                this.setState({ dateError: "" })
            } else {
                this.setState({ dateError: "This is a past date" })

            }

        }

    }
    checkTime = (value) => {
        console.log(value)
        if (value === "") {
            this.setState({ TimeError: "Please Select a time" })
        } else {
            this.setState({ TimeError: "" })
        }
    }
    render() {
        const { toggleCollapse } = this.props
        return (


            <div className="" >
                <Row>
                    <Col md="6">
                        <FormGroup >
                            <CustomInput
                                type="radio"
                                className="h5"
                                name="isDesignRadio"
                                id="uploadCheckbox"
                                onChange={(e) => {
                                    this.setState({ designFileUrl: "" });
                                    this.toggle();
                                    // this.setState({ isDesign: e.target.checked }) 
                                }}
                                checked={!this.state.isDesign}

                                label="Upload your AD design" ></CustomInput>
                        </FormGroup>
                        <Collapse isOpen={!this.state.isDesign}>
                            <FormGroup>
                                <Label tag="h6" for="exampleCustomFileBrowser" >Upload your AD design</Label>
                                <CustomInput
                                    type="file"
                                    id="exampleCustomFileBrowser"
                                    name="customFile"
                                    
                                    onChange={(e) => {
                                        
                                        this.setState({ AdUrl: e.target.files[0] })
                                        
                                    }
                                    }
                                    label="Upload your Ad Design" />
                                    <label>{this.state.AdUrl===undefined?"No file chosen":this.state.AdUrl.name}</label>
                            </FormGroup>
                        </Collapse>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <div className="h5 text-center font-weight-light">or</div>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup>
                            <CustomInput

                                type="radio"
                                className="h5"
                                name="isDesignRadio"
                                id="isDesignCheckbox"
                                onChange={(e) => {
                                    this.setState({ AdUrl: "" });
                                    this.toggle();
                                    // this.setState({ isDesign: e.target.checked }) 
                                }}

                                label="Request to Design an AD" ></CustomInput>
                        </FormGroup>

                        <div></div>
                        <Collapse className="bg-" isOpen={this.state.isDesign} tag="h5" className="mt-3">
                            <Row>
                                <Col md="8">
                                    <FormGroup>
                                        <Label className="h6" for="exampleCustomFileBrowser">Upload Images(If any)</Label>
                                        <CustomInput
                                            type="file" id="exampleCustomFileBrowser"
                                            name="customFile"
                                            label="Upload a Image"
                                            onChange={(e) => { this.setState({ designFileUrl: e.target.files[0] }) }}
                                        ></CustomInput>
                                        <Label>{this.state.designFileUrl===undefined?"No file chosen":this.state.designFileUrl.name}</Label>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label className="h6" for="exampleText">Text to display in the AD</Label>
                                        <Input
                                            onChange={(e) => { this.setState({ textTODisplay: e.target.value }) }}
                                            type="textarea"
                                            name="text"
                                            id="exampleText"
                                            placeholder="Text here" />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <FormGroup>
                                <CustomInput
                                    onClick={this.ContactToggle}
                                    type="checkbox"
                                    id="isContactCheckbox"
                                    onChange={(e) => { this.setState({ isContact: e.target.checked }) }}
                                    label="Request designer to call you for the instructions" ></CustomInput>
                            </FormGroup>
                            <Fade in={this.state.isContact}>
                                <Row>
                                    <Col md="8">
                                        <Label className="h6" for="tele">Enter your contact number</Label>
                                        <Input invalid={this.state.noContactNum}
                                            type="tel"
                                            id="tele"
                                            name="telephone"
                                            placeholder="Contact number"
                                            onChange={(e) => {
                                                this.setState({ contactNum: e.target.value })
                                                this.checkTelNumber(e.target.value)
                                            }}
                                            onBlur={(e) => {
                                                this.checkTelNumber(e.target.value)
                                            }}
                                        ></Input>
                                        <FormFeedback className="animated shake">{this.state.contactNumError}</FormFeedback>
                                    </Col>

                                </Row>
                                <Row>
                                    <Col md="4">
                                        <Label className="h6" for="piackadate">Pick a Date</Label>
                                        <Input
                                            invalid={this.state.dateError === "" ? false : true}
                                            type="date"
                                            name="date"
                                            id="pickadate"
                                            placeholder="Pick a Date"
                                            onBlur={(e) => {

                                                this.checkDate(e.target.value)
                                            }}
                                            onChange={(e) => {
                                                this.setState({ date: e.target.value })
                                                this.checkDate(e.target.value)
                                            }}
                                        />
                                        <FormFeedback className="animated shake">{this.state.dateError}</FormFeedback>
                                    </Col>
                                    <Col md="4">
                                        <Label className="h6" for="pickatime">Pick a Time</Label>
                                        <Input
                                            invalid={this.state.TimeError === "" ? false : true}
                                            type="time"
                                            name="time"
                                            id="pickatime"
                                            placeholder="Pick a Time"
                                            onBlur={(e) => {

                                                this.checkTime(e.target.value)
                                            }}
                                            onChange={(e) => {
                                                this.setState({ time: e.target.value })
                                                this.checkTime(e.target.value)
                                            }
                                            }
                                        />
                                        <FormFeedback className="animated shake">{this.state.TimeError}</FormFeedback>
                                    </Col>
                                </Row>
                            </Fade>
                        </Collapse>
                        <Button className="btn-next"
                            disabled={

                                this.state.isDesign === true && this.state.isContact===true && (this.state.contactNum === "" || this.state.date === "" || this.state.time === "") ?
                                    true
                                    : (this.state.isDesign===true && (this.state.designFileUrl===""||this.state.textTODisplay==="")?
                                    true:(this.state.isDesign === false && (this.state.AdUrl === "") ? true : false)
                                    )
                                    
                                    
                                    




                            }


                            color="success "
                            size="lg"
                            onClick={() => {
                                // ChangeStep("4");
                                this.submit()
                                toggleCollapse(4)
                                // if (this.state.isContact === true && this.state.contactNum !== "") {
                                //     ChangeStep("4")
                                // }
                                // if (this.state.isDesign === false && this.state.designFile !== "") {
                                //     ChangeStep("4")
                                // }
                            }}
                        >Next</Button>

                    </Col>
                </Row>
            </div>



        )
    }

    ContactToggle() {
        this.setState({
            isContact: !this.state.isContact
        })
    }
    toggle() {
        this.setState({
            isDesign: !this.state.isDesign
        });
    }
}

export default UploadAd;