import React from "react";
import { Card, Button, CardTitle, CardText, Col, Row, Spinner } from 'reactstrap';
import "./package-style.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Error from "../loadingError/Error"
class FacebookGroups extends React.Component {
    constructor() {
        super();
        this.state = {
            Packages: [],
            route: "loading"
        }
    }
getData=()=>{
    this.setState({ Packages:"", route: "loading" })

    fetch("https://th.threadzlab.com/getFbGroupAdData.php")
    .then(res => res.json())
    .then(data => {
        //this.setState({ data })
        this.setState({ Packages: data, route: "done" })
    }).catch(e=>{
        this.setState({ Packages:"", route: "error" })

    })

}
    componentWillMount() {
        
this.getData()
    }

    render() {


       
        const { toggleCollapse, ChangeSelectedPackage } = this.props

        return (
            <div className="">
                {
                    this.state.route === "loading" ?
                        <div>
                            <div className="paymentLoading">
                                <Spinner color="primary" className="paymentSpinner"></Spinner>
                            </div>
                        </div>
                        :(this.state.route==="error"?
                        <Error reloadFunction={this.getData}></Error>
                        :
                        <Row className="animated fadeInUp">
                            {this.state.Packages.map((pack, i) => {
                                console.log(pack)
                                return (
                                    <Col sm={3} className="m-2">
                                        <Card body className="pack" onClick={() => {
                                            toggleCollapse(3);
                                            ChangeSelectedPackage(this.state.Packages[i]);
                                        }} >
                                            <CardTitle className="packageCardTitle">{pack.Pack_Name}</CardTitle>
                                            <hr></hr>
                                            <CardText>
                                                <div className="packDataList">
                                                    <li>
                                                        <a  className="cardDataTitle  h6">
                                                        <FontAwesomeIcon className="m-1 " size="2x" icon={"users"}></FontAwesomeIcon>
                                                       <br></br>  Members</a>
                                                        <a className="cardDataData h2">{pack.Members}</a>
                                                    </li>
                                                    <li>
                                                        <a className="cardDataTitle  h6 " >
                                                        <FontAwesomeIcon className="m-1" size="2x" icon={"running"}></FontAwesomeIcon>
                                                      <br></br>Reach</a>
                                                        <a className="cardDataData h2">{pack.Reach}</a>
                                                    </li>
                                                    <li>
                                                        <a className="cardDataTitle  h6 ">
                                                        <FontAwesomeIcon className="m-1" size="2x" icon={"stopwatch"}></FontAwesomeIcon>
                                                      <br></br>Duration</a>
                                                        <a className="cardDataData h2">{pack.Duration} days</a>
                                                    </li>
                                                    <li>
                                                        <a className="cardDataTitle
                                                          h6">
                                                        <FontAwesomeIcon className="m-1" size="2x" icon={"tag"}></FontAwesomeIcon>
                                                       <br></br>Price</a>
                                                        <a className="cardDataData h3">{pack.Price} LKR</a>
                                                    </li>
                                                </div>
                                            </CardText>

                                        </Card>
                                    </Col>)
                            })}


                        </Row>
                        )
                        
                }


            </div>
        )
    }
}


export default FacebookGroups;