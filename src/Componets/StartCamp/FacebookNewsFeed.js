import React from "react";
import { Card, Button, CardTitle, CardText, Col, Row, Spinner } from 'reactstrap';
import "./startCamp.css"
import image from "./newsfeed.png"
import "./package-style.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Error from "../loadingError/Error"
class FacebookNewsFeed extends React.Component {
    constructor() {
        super();
        this.state = {
            Packages: [],
            route: "loading"
        }
    }
    getData=()=>{
        this.setState({ Packages:"", route: "loading" })
    
        fetch("https://th.threadzlab.com/getNewsFeedAdData.php")
            .then(res => res.json())
            .then(data => {
                this.setState({ Packages: data, route: "done" })

            }).catch(e=>{
            this.setState({ Packages:"", route: "error" })
    
        })
    
    }
    componentWillMount = () => {
       this.getData();
    }

    render() {
        const { toggleCollapse, ChangeSelectedPackage } = this.props
       
         
        return (
            <div className="">
                {this.state.route === "loading" ?
                    <div>
                        <div className="paymentLoading">
                            <Spinner color="primary" className="paymentSpinner"></Spinner>
                        </div>
                    </div>
                    :(this.state.route==="error"?
                    <Error reloadFunction={this.getData}></Error>
                    :
                    <div>
                        <Row>
                            <Col sm="9" className="align-middle">
                                <h5>Display your ads on Facebook News Feed as sponsored ads.</h5>
                            </Col>
                        </Row>

                        <Row className="animated fadeInUp">
                            {this.state.Packages.map((pack, i) => {
                                return (
                                    <Col className=" m-2" sm='3'>

                                       
                                        <Card body className="pack" onClick={() => {
                                            toggleCollapse(3);
                                            ChangeSelectedPackage(this.state.Packages[i]);
                                        }} >
                                            <CardTitle className="packageCardTitle">{pack.Pack_Name}</CardTitle>
                                            <hr></hr>
                                            <CardText>
                                                <div className="packDataList">
                                                   
                                                    <li>
                                                        <a className="cardDataTitle  h6 " >
                                                        <FontAwesomeIcon className="m-1" size="2x" icon={"running"}></FontAwesomeIcon>
                                                      <br></br>Reach</a>
                                                        <a className="cardDataData h2">{pack.Reach}</a>
                                                    </li>
                                                    <li>
                                                        <a className="cardDataTitle  h6 ">
                                                        <FontAwesomeIcon className="m-1" size="2x" icon={"stopwatch"}></FontAwesomeIcon>
                                                      <br></br>Duration</a>
                                                        <a className="cardDataData h2">{pack.Duration} days</a>
                                                    </li>
                                                    <li>
                                                        <a className="cardDataTitle  h6">
                                                        <FontAwesomeIcon className="m-1" size="2x" icon={"tag"}></FontAwesomeIcon>
                                                       <br></br>Price</a>
                                                        <a className="cardDataData h3">{pack.Price} LKR</a>
                                                    </li>
                                                </div>
                                            </CardText>

                                        </Card>
                                    </Col>
                                )
                            })}


                        </Row>
                        <Row className="animated fadeInUp">
                            <Col sm='9'>
                                <img className="align-center" src={image}></img>
                            </Col>

                        </Row>
                    </div>
                    )
                    
                }

            </div>
        )
    }
}


export default FacebookNewsFeed;