import React from "react";
import { Card, Button, CardTitle, CardText, CardImg,Row, Col } from 'reactstrap';
import "./methods-style.css"
import imgNewsfeed from "../assets/newsfeed.webp"
import imgFbGroup from "../assets/groupFB.webp"
import imgFbPage from "../assets/page.webp"
import imgInApp from "../assets/app.webp"

class Methods extends React.Component {
    constructor(props) {
        super();
        this.state = {


        }
    }

    render() {
        const {  ChangeSelectedMethod,toggleCollapse } = this.props
        return (

            <div>

                <div className=" ">
                    <Row className="ma-2">
                        <Col sm="3">
                            <Card className="methodCard align-middle" onClick={() => { toggleCollapse(2); ChangeSelectedMethod("1") }} body outline >
                                <CardTitle>
                                   
                                    <b>Facebook<br></br> Newsfeed ADS</b> </CardTitle>
                                    <hr></hr>
                                    <CardImg className="cardimg" src={imgNewsfeed}></CardImg>
                                <CardText>Show your advertisements on facebook News-Feed</CardText>
                                {/* <Button
                                className="cardbutton"
                                    onClick={() => { ChangeStep("2"); ChangeSelectedMethod("1") }}
                                    
                                    
                                >Go somewhere</Button> */}
                            </Card>
                        </Col>
                        <Col sm="3">
                            <Card className="methodCard align-middle" onClick={() => { toggleCollapse(2); ChangeSelectedMethod("2") }} body>
                                <CardTitle><b>Facebook<br></br> Group ADS</b></CardTitle>
                                <hr></hr>

                                <CardImg className="cardimg"  src={imgFbGroup}></CardImg>
                                <CardText>Target your audience more effectively</CardText>
                            </Card>
                        </Col>
                        <Col sm="3">
                            <Card className="methodCard align-middle" onClick={() => { toggleCollapse(2); ChangeSelectedMethod("3") }}body >
                                <CardTitle><b>Facebook<br></br> Pages ADS</b></CardTitle>
                                <hr></hr>

                                <CardImg className="cardimg"  src={imgFbPage}></CardImg>
                              <CardText>Publish your advertisements through Facebook pages</CardText>
                            </Card>
                        </Col>
                        <Col sm="3">
                            <Card className="methodCard align-middle" onClick={() => {toggleCollapse(2); ChangeSelectedMethod("4") }}body >
                                <CardTitle><b>In Educational<br></br> App ADS</b></CardTitle>
                                <hr></hr>

                                <CardImg className="cardimg"  src={imgInApp}></CardImg>
                               <CardText>Show your advertisements in popular Educational apps</CardText>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}


export default Methods;