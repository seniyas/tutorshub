import React from "react";
import { Button, Form, FormGroup, Label, Input, Row, Col, Collapse, CustomInput, FormFeedback } from 'reactstrap';
import "./startCamp.css"
class Schedule extends React.Component {
    constructor() {
        super();
        this.state = {
            isFixedDate: true,
            caption: "",
            keywords: "",
            audience: "",
            city1: "",
            city2: "",
            city3: "",
            campaignDate: "",

            captionError: "",
            audienceError: "",
            keywordError: "",
            campaignDateError: "",
            cityError: ""
        }
    }
    toggleFixedDate = () => {
        this.setState({ isFixedDate: !this.state.isFixedDate })
    }
    checkCaption = (value) => {
        value.length > 8 ?
            this.setState({ captionError: "", caption: value })
            :
            this.setState({ captionError: "Please enter a caption (Caption must contain more than 8 characters )" })
    }
    checkKeywords = (value) => {
        var comma = /[,]/
        if (comma.test(value)) {
            this.setState({ keywordError: "", keywords: value })
        } else {
            this.setState({ keywordError: "Please enter at least two key words separated by commas", keywords: "" })
        }
    }
    checkDate = (value, isFixed) => {
        if (value !== "" && isFixed === true) {
            var nowDate = new Date();
            var checkDate = new Date(value);
            if (nowDate > checkDate) {

                if (nowDate.getDate() === checkDate.getDate() && nowDate.getMonth() === checkDate.getMonth() && nowDate.getFullYear() === checkDate.getFullYear()) {
                    this.setState({ campaignDateError: "", campaignDate: value })
                } else {
                    this.setState({ campaignDateError: "This is a past date", campaignDate: "" })

                }
            } else {

                this.setState({ campaignDateError: "", campaignDate: value })
            }
        } else if (value === "" && isFixed == false) {
            this.setState({ campaignDateError: "Please describe campaign date", campaignDate: "" })
        } else {
            this.setState({ campaignDateError: "", campaignDate: value })
        }
    }
    checkAudience = (value) => {
        value === "Select a target audience" ?
            this.setState({ audienceError: "Please select what audience are you going to target", audience: "" })
            :
            this.setState({ audienceError: "", audience: value })
    }
    checkCity = (value) => {
        value === "Select a city" ?
            this.setState({ cityError: "Please select at least one target city" })
            :
            this.setState({ cityError: "" })
    }
    submit = () => {
        const { isFixedDate,
            caption,
            keywords,
            audience,
            city1,
            city2,
            city3,
            campaignDate } = this.state
        let data = {
            isFixedDate: isFixedDate,
            caption: caption,
            keywords: keywords,
            audience: audience,
            city1: city1,
            city2: city2,
            city3: city3,
            campaignDate: campaignDate,
        }
        this.props.ChangeScheduleData(data)
    }
    render() {
        const { toggleCollapse } = this.props
        return (
            <div className=" h5 ">
                <Form>
                    <FormGroup>
                        <Label for="AdCaption">Your Ad Caption</Label>
                        <Input
                            invalid={this.state.captionError === "" ? false : true}
                            type="text"
                            name="AdCaption"
                            id="AdCaption"
                            placeholder="Ex:.... classes for 2020 A/L Students"
                            onBlur={(e) => { this.checkCaption(e.target.value) }}
                            onChange={(e) => {
                                this.checkCaption(e.target.value)

                            }}
                        />
                        <FormFeedback className="animated shake">{this.state.captionError}</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                        <Label for="keywords">Keywords</Label>
                        <Input
                            type="text"
                            invalid={this.state.keywordError === "" ? false : true}
                            name="keywords"
                            id="keywords"
                            placeholder="Ex:A/L students,biology,2020 A/L...."
                            onBlur={(e) => { this.checkKeywords(e.target.value) }}
                            onChange={(e) => {
                                this.checkKeywords(e.target.value)
                            }}

                        />
                        <FormFeedback className="animated shake">{this.state.keywordError}</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                        <Row >
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleSelect">Target Audience</Label>
                                    <Input
                                        invalid={this.state.audienceError === "" ? false : true}
                                        onBlur={(e) => { this.checkAudience(e.target.value) }}
                                        onChange={(e) => { this.checkAudience(e.target.value) }} type="select" name="select" id="exampleSelect">
                                        <option >Select a target audience</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Input>
                                    <FormFeedback>{this.state.audienceError}</FormFeedback>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md={2}>
                                <Label for="TargetCity1">Target City-1</Label>
                                <Input onChange={(e) => { this.setState({ city1: e.target.value }); this.checkCity(e.target.value) }} type="select" name="city1" id="TargetCity1">
                                    <option>Select a city</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </Input>


                            </Col>
                            <Col md={2}>
                                <Label for="exampleSelect">Target City-2</Label>
                                <Input onChange={(e) => { this.setState({ city2: e.target.value }); this.checkCity(e.target.value) }} type="select" name="city2" id="TargetCity2">
                                    <option>Select a city</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </Input>
                            </Col>
                            <Col md={2}>
                                <Label for="exampleSelect">Target City-3</Label>
                                <Input onChange={(e) => { this.setState({ city3: e.target.value }); this.checkCity(e.target.value) }} type="select" name="city3" id="TargetCity3">
                                    <option>Select a city</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </Input>
                            </Col>

                            <FormFeedback>{this.state.cityError}</FormFeedback>
                        </Row>
                    </FormGroup>
                    <FormGroup >
                        <CustomInput
                            type="radio"
                            id="fixedDate"
                            name="customRadio"
                            label="On Fixed Date"
                            // onClick={this.toggleFixedDate}
                            onChange={(e) => { this.toggleFixedDate(); this.setState({ campaignDate: "", campaignDateError: "" }) }}
                            value="fixed"
                            checked={this.state.isFixedDate}
                        />
                        <Collapse isOpen={this.state.isFixedDate}>

                            <FormGroup>
                                <Label className="h6" for="fixedDateSelect">Select a fixed date</Label>
                                <Col md={6}>
                                    <Input
                                        invalid={this.state.campaignDateError === "" ? false : true}
                                        type="date"
                                        id="fixedDateSelect"
                                        name="selectFixedDate"
                                        onBlur={(e) => { this.checkDate(e.target.value, true) }}
                                        onChange={(e) => { this.checkDate(e.target.value, true) }}
                                    ></Input>
                                    <FormFeedback className="animate shake">{this.state.campaignDateError}</FormFeedback>
                                </Col>

                            </FormGroup>
                        </Collapse>
                        <CustomInput
                            type="radio"
                            id="SpecificDate"
                            name="customRadio"
                            label="On Specific Date"
                            value="specific"
                            // onClick={(e)=>{this.toggleFixedDate();console.log(e.target.value) }}
                            onChange={(e) => { this.toggleFixedDate(); this.setState({ campaignDate: "", campaignDateError: "" }) }}
                        />
                        <Collapse isOpen={!this.state.isFixedDate}>
                            <FormGroup>
                                <Label className="h6" for="specificDateSelect">Describe your fixed date</Label>
                                <Col md={6}>
                                    <Input
                                        invalid={this.state.campaignDateError === "" ? false : true}
                                        name="specificDate"
                                        id="specificDateSelect"
                                        placeholder="Ex:When GCE O/L results release"
                                        onBlur={(e) => { this.checkDate(e.target.value, false) }}
                                        onChange={(e) => { this.checkDate(e.target.value, false) }}
                                    ></Input>
                                    <FormFeedback className="animated shake">{this.state.campaignDateError}</FormFeedback>
                                </Col>
                            </FormGroup>
                        </Collapse>
                    </FormGroup>
                    <Button
                        disabled={
                            this.state.caption === "" || this.state.keywords === "" || this.state.audience === "" || this.state.campaignDate === "" ?
                                true
                                :
                                false

                        }
                        className="btn-next"
                        color="success "
                        size="lg"
                        onClick={() =>{this.submit();toggleCollapse(5) } }
                    >Next</Button>
                </Form>
            </div>
        )
    }
}


export default Schedule;