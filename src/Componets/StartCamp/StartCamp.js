import React from "react";
import Method from "../StartCamp/Methods"
import SelectPackage from "../StartCamp/SelectPackage"
import Upload from "../StartCamp/Upload"
import SideList from "../StartCamp/SideList"
import Schedule from "../StartCamp/Schedule"
import { Col, Row, Collapse, Button } from "reactstrap"
import Payment from "./Payment"
import FbNewsFeed from "./FacebookNewsFeed"
import FacebookPage from "./FacebookPage"
import FacebookGroup from "./FacebookGroup"
import InAppAds from "./InAppAds"
import "./startCamp.css"
import "animate.css"
import { stringify } from "querystring";
import firebase from "firebase"
import InfoPannel from "./infoPannel"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class StartCamp extends React.Component {
    constructor() {
        super();
        this.state = {
            step: '1',
            selectedMethod: "",
            paymentRoute: "payment",
            payment: "pending",
            selectedPackage: {
                Pack_ID:"",
                Prize:"",
                name:"",
                duration:""
            },
            ADDesigner: {
                file: "",
                text: "",
                contactDesigner: Boolean,
                contactNum: Number,
                date: String,
                time: String
            },
            Campaign: {
                file: "",
                caption: String,
                keywords: String,
                audience: String,
                city1: String,
                city2: String,
                city3: String
            },
            CampUploadData: {
                isDesign: "",
                textOnDesign: "",
                isContact: "",
                contactNum: "",
                contactDate: "",
                contactTime: "",
                AdDesign: "",
                designFile: "",
            },
            ScheduleData: {
                isFixedDate: "",
                caption: "",
                keywords: "",
                audience: "",
                city1: "",
                city2: "",
                city3: "",
                campaignDate: "",
            },
            AdDesignURL: "",
            userProfile: {
                uid: "",
                name: "",
                email: ""
            },
            collapseMethod: true,
            collapsePack: false,
            collapseUpload: false,
            collapseSchedule: false,
            collapsePayment: false


        }
    }
    componentWillMount = () => {
        var user = firebase.auth().currentUser;
        //console.log(user)

        if (user) {
            this.setState({
                userProfile: {
                    uid: user.uid,
                    name: user.displayName,
                    email: user.email
                }
            })
        } else {
            // No user is signed in.
            this.props.toastChange("Your session timeout", "Something went wrong please try refreshing the browser", true)
        }

    }
    uploadImage = (file) => {
        this.setState({ paymentRoute: "loading" })
        // console.log("Upload I=mage Run", file);
        // console.log("submitCApm run", this.state.CampUploadData.AdDesign)
        //    this.props.uploadImage(this.state.CampUploadData.AdDesign)
        let sotrageRef = firebase.storage().ref().child(`${this.props.uid}/${file.name}`)
        sotrageRef.put(file).then((snapshot) => {
            console.log("Uploaded!")


        }).then(() => {
            let downloadURL = sotrageRef.getDownloadURL().then((url) => {
                this.setState({ AdDesignURL: url })
                //console.log(this.state.AdDesignURL)
            }).then(() => {
                console.log(this.props.uid)
                const { CampUploadData, ScheduleData, selectedPackage } = this.state
                var today = new Date();
                var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                var dateTime = date + ' ' + time;
                console.log(dateTime)
                fetch(`https://th.threadzlab.com/insertCampOld.php?Caption=${ScheduleData.caption}
            &design_URL="eee"
            &UID=${this.props.uid}
            &keywords=${ScheduleData.keywords}
            &audience=${ScheduleData.audience}
            &city1=${ScheduleData.city1}
            &city2=${ScheduleData.city2}
            &city3=${ScheduleData.city3}
            &scheduled_date=${ScheduleData.campaignDate}
            &isDesigning=${CampUploadData.isDesign === true ? "1" : "0"}
            &payment_amount=${selectedPackage.Prize}
            &payment_status=${this.state.payment}
            &selected_package=${this.state.selectedMethod + "," + selectedPackage.Pack_ID}
            &date_created=${dateTime}
            `).then(res => res.json())
                    .then(data => {
                        console.log(data);
                        if (data && data.result === "done" && CampUploadData.isDesign === true) {
                            console.log("sending data t designer table")
                            let campID = data.campId
                            fetch(`https://th.threadzlab.com/insertADDesignerData.php?
                            Camp_ID=${campID}
                            &Image_url=${encodeURIComponent(this.state.AdDesignURL)}
                            &textsInAd=${CampUploadData.textOnDesign}
                            &isContact=${CampUploadData.isContact === true ? "1" : "0"}
                            &contactNumber=${CampUploadData.contactNum}
                            &contactTime=${CampUploadData.contactTime}
                            &ContactDate=${CampUploadData.contactDate}`)
                                .then(res => res.json())
                                .then(data => {
                                    console.log(data);
                                    if (data.result === "done") {
                                        this.setState({ paymentRoute: "done" })
                                        setTimeout(() => {
                                            this.props.ChangeRoute("dashboard");
                                        }, 3000);


                                    }
                                }).catch(error => this.setState({ paymentRoute: "failed" }))
                        } else if (CampUploadData.isDesign === false) {
                            this.setState({ paymentRoute: "done" })
                            setTimeout(() => {
                                this.props.ChangeRoute("dashboard");
                            }, 3000);
                        }



                    }).catch(error => this.setState({ paymentRoute: "failed" }))
            }).catch(error => this.setState({ paymentRoute: "failed" }))

        }).catch(error => this.setState({ paymentRoute: "failed" }))


    }
    ChangeSelectedPackage = (Package) => {
        this.setState({
            selectedPackage: Package
        })
    }
    ChangeStep = (step) => {
        this.setState({ step: step })
    }

    ChangeSelectedMethod = (Method) => {
        this.setState({ selectedMethod: Method })
    }
    ChangeCampData = (data) => {
        this.setState({ CampUploadData: data })
    }

    ChangeScheduleData = (data) => {
        console.log(data)
        this.setState({ ScheduleData: data })
    }

    submitCamp = () => {
        if (this.state.CampUploadData.isDesign === true) {
            this.uploadImage(this.state.CampUploadData.designFile)
        } else {
            this.uploadImage(this.state.CampUploadData.AdDesign)
        }


    }
    toggleCollapse = (target) => {
        switch (target) {
            case 1:
                this.setState({
                    collapseMethod: true,
                    collapsePack: false,
                    collapseUpload: false,
                    collapseSchedule: false,
                    collapsePayment: false
                })
                break;
            case 2:
                this.setState({
                    collapseMethod: false,
                    collapsePack: true,
                    collapseUpload: false,
                    collapseSchedule: false,
                    collapsePayment: false
                })
                break;
            case 3:
                this.setState({
                    collapseMethod: false,
                    collapsePack: false,
                    collapseUpload: true,
                    collapseSchedule: false,
                    collapsePayment: false
                })
                break;
            case 4:
                this.setState({
                    collapseMethod: false,
                    collapsePack: false,
                    collapseUpload: false,
                    collapseSchedule: true,
                    collapsePayment: false
                })
                break;
            case 5:
                this.setState({
                    collapseMethod: false,
                    collapsePack: false,
                    collapseUpload: false,
                    collapseSchedule: false,
                    collapsePayment: true
                })
                break;

        }

    }
    SelectedMethodInText=(method)=>{

     switch (method) {
            case "1":
                return "Facebook News Feed Advertisement"

            case "2":
                return "Facebook Group Advertisement"
            case "3":
                return "Facebook Page Advertisement"
            case "4":
                return "In Apps"
            default:
            return ""
        }
    }

    render() {
        var time = new Date().getHours()
        var step = this.state.step
        return (
            <div className="div">
                <Row >
                    {/* <Col className=" animated bounceInLeft sidelist p-0 h6" xs="3" >
                        <SideList className="animated bounceInLeft" Step={this.state.step}></SideList>
                    </Col> */}
                    <Col className="p-0 ">

                        {/*  */}
                        <div className="cardcontainer">
                            <InfoPannel
                             uploadData={this.state.CampUploadData}
                             ScheduleData={this.state.ScheduleData.caption}
                            selectedMethod={this.state.selectedMethod}
                            step={this.state.step}
                            name={this.state.userProfile.name}
                            selectedPackage={this.state.selectedPackage}
                            changeStep={this.ChangeStep}
                        ></InfoPannel>
                                </div>
                        <div className="flex-container" >
                            <div className=" startCampContainer">


                                <div className={`formdiv ${this.state.collapseMethod ? "formdivOpen" : ""}`} onClick={() => this.toggleCollapse(1)}>
                                    <FontAwesomeIcon icon={"chevron-down"} className="float-right"></FontAwesomeIcon>
                                    Select advertising method: <a className="text-success">{this.SelectedMethodInText(this.state.selectedMethod)}</a>
                                </div>
                                <Collapse isOpen={this.state.collapseMethod} >
                                    <div className="formContainer">
                                        <Method ChangeSelectedMethod={this.ChangeSelectedMethod}

                                            toggleCollapse={this.toggleCollapse}
                                        ></Method>
                                    </div>
                                </Collapse>

                                <div className={`formdiv ${this.state.collapsePack ? "formdivOpen" : ""}`} onClick={() => this.toggleCollapse(2)}>
                                    <FontAwesomeIcon icon={"chevron-down"} className="float-right"></FontAwesomeIcon>
                                    Select a package
                                </div>
                                <Collapse isOpen={this.state.collapsePack}>
                                    {this.state.selectedMethod === "1" ?
                                        <FbNewsFeed
                                            ChangeSelectedPackage={this.ChangeSelectedPackage}
                                            toggleCollapse={this.toggleCollapse}
                                        ></FbNewsFeed>
                                        : this.state.selectedMethod === "2" ?
                                            <FacebookGroup
                                                ChangeSelectedPackage={this.ChangeSelectedPackage}
                                                toggleCollapse={this.toggleCollapse}
                                            ></FacebookGroup>
                                            : this.state.selectedMethod === "3" ?
                                                <FacebookPage
                                                    ChangeSelectedPackage={this.ChangeSelectedPackage}
                                                    toggleCollapse={this.toggleCollapse}
                                                ></FacebookPage>
                                                :
                                                <InAppAds
                                                    ChangeSelectedPackage={this.ChangeSelectedPackage}
                                                    toggleCollapse={this.toggleCollapse}
                                                ></InAppAds>
                                    }
                                </Collapse>

                                <div className={`formdiv ${this.state.collapseUpload ? "formdivOpen" : ""}`} onClick={() => this.toggleCollapse(3)}>
                                    <FontAwesomeIcon icon={"chevron-down"} className="float-right"></FontAwesomeIcon>
                                    Upload your advertisement 
                                </div>
                                <Collapse isOpen={this.state.collapseUpload}>
                                    <div className="formContainer ">

                                        <Upload
                                            ChangeCampData={this.ChangeCampData}
                                            toggleCollapse={this.toggleCollapse}
                                        ></Upload>
                                    </div>
                                </Collapse>

                                <div className={`formdiv ${this.state.collapseSchedule ? "formdivOpen" : ""}`} onClick={() => this.toggleCollapse(4)} >
                                    <FontAwesomeIcon icon={"chevron-down"} className="float-right"></FontAwesomeIcon>
                                    Schedule the campaign
                                </div>
                                <Collapse isOpen={this.state.collapseSchedule}>
                                    <div className="formContainer ">
                                        <Schedule
                                            ChangeScheduleData={this.ChangeScheduleData}
                                            toggleCollapse={this.toggleCollapse}></Schedule>
                                    </div>
                                </Collapse>

                                <div className={`formdiv ${this.state.collapsePayment ? "formdivOpen" : ""}`} onClick={() => this.toggleCollapse(5)} >
                                    <FontAwesomeIcon icon={"chevron-down"} className="float-right"></FontAwesomeIcon>
                                    Choose a payment method
                                </div>
                                <Collapse isOpen={this.state.collapsePayment}>
                                    <div className="formContainer ">
                                        <Payment
                                            uploadData={this.state.CampUploadData}
                                            ScheduleData={this.state.ScheduleData.caption}
                                            selectedPackage={this.state.selectedPackage}
                                            submitCamp={this.submitCamp}
                                            paymentRoute={this.state.paymentRoute}
                                            selectedMethod={this.state.selectedMethod}
                                        ></Payment>
                                    </div>
                                </Collapse>
                            </div>


                        </div>
                    </Col>

                </Row>


            </div>

        )
    }
}


export default StartCamp;