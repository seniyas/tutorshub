import React from "react";
import { Badge,ListGroup, NavItem, NavLink, ListGroupItem, Nav } from 'reactstrap';
import "./startCamp.css"
import "animate.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class SideList extends React.Component {
  constructor(props) {
    super();
  }


  render() {
    const { Step } = this.props
   // console.log(Step)
    return (
      <div className="pt-2  ">
        <Nav vertical >
          <ListGroup  >
            <ListGroupItem className="" active={Step === "1" ? true : false} tag="a" href="#"action><FontAwesomeIcon icon={"hand-pointer"} size="lg" fixedWidth ></FontAwesomeIcon>  Select Advertising Method</ListGroupItem>
            <ListGroupItem className="" active={Step === "2" ? true : false} tag="a" href="#" action><FontAwesomeIcon icon={"box-open"} size="lg" fixedWidth ></FontAwesomeIcon>  Choose a Package</ListGroupItem>
            <ListGroupItem className="" active={Step === "3" ? true : false} tag="a" href="#" action><FontAwesomeIcon icon={"upload"} size="lg" fixedWidth ></FontAwesomeIcon>  Upload your AD/Make design request</ListGroupItem>
            <ListGroupItem className="" active={Step === "4" ? true : false} tag="a" href="#" action><FontAwesomeIcon icon={"clock"} size="lg" fixedWidth ></FontAwesomeIcon>  Schedule your Campaign</ListGroupItem>
            <ListGroupItem className="" active={Step === "5" ? true : false} tag="a" href="#" action><FontAwesomeIcon icon={"money-check-alt"} size="lg" fixedWidth ></FontAwesomeIcon>  Choose a payment method</ListGroupItem>
          </ListGroup>
          {/* <NavItem>
            <NavLink href="#">Link</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="#">Link</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="#">Another Link</NavLink>
          </NavItem>
          <NavItem>
            <NavLink disabled href="#">Disabled Link</NavLink>
          </NavItem> */}
        </Nav>
      </div>
    )
  }
}


export default SideList;