import React from "react";
import { CardText, Button, Spinner, CardSubtitle, CardTitle, CardBody, Card, Col, Row, Collapse, InputGroupAddon, InputGroupText, Input, InputGroup } from "reactstrap"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import "./payment.css"
class Payment extends React.Component {
    constructor() {
        super();
        this.state = {
            route: "payment",
            payment: "pending",
            showBankData: false,

        }
    }
    showSelectedMethod = (method) => {
        switch (method) {
            case "1":
                return "Facebook News Feed Advertisement"

            case "2":
                return "Facebook Group Advertisement"
            case "3":
                return "Facebook Page Advertisement"
            case "4":
                return "In Apps Advertisement"

        }
    }
    toggleBankData() {
        this.setState({
            showBankData: !this.state.showBankData
        });
    }
    render() {
        const { paymentRoute, selectedPackage, selectedMethod, ScheduleData, uploadData } = this.props
        const packPrice = selectedPackage.Price;
        const DesigningFee = 0;
        const total = Number(packPrice) + Number(DesigningFee)

        return (
            <div className="">
                {paymentRoute === "payment" ?
                    <div>
                        <Row>

                            <Col md={12} className=" ">
                                <h1>Invoice</h1>

                                <div className="invoiceContainer">
                                    <div className="invoiceHeader">Package details</div>
                                    <div>
                                        <div className="list">
                                            <li className="invoiceDetails">
                                                <a>Advertising method:</a>
                                                <a className="invoiceData">{this.showSelectedMethod(selectedMethod)}</a>
                                            </li>

                                            <li className="invoiceDetails">
                                                <a>Package Name:</a>
                                                <a className="invoiceData">{selectedPackage.Pack_Name}</a>
                                            </li>
                                            <li className="invoiceDetails">
                                                <a>Valid time period:</a>
                                                <a className="invoiceData">{selectedPackage.Duration}days</a>
                                            </li>
                                            <li className="invoiceDetails">
                                                <a>Reach:</a>
                                                <a className="invoiceData">{selectedPackage.Reach ? selectedPackage.Reach : "N/A"}</a>
                                            </li>
                                        </div>
                                    </div>
                                    <br></br>
                                    <div className="invoiceHeader">Payment Info</div>
                                    <div>
                                        <div className="list">
                                            <li className="invoiceDetails">
                                                <a>Package Prize:</a>
                                                <a className="invoiceData">{"LKR " + packPrice}</a>
                                            </li>

                                            <li className="invoiceDetails">
                                                <a>Designer Fee:</a>
                                                <a className="invoiceData">{`${DesigningFee === "0" ? "" : "LKR "}${DesigningFee}`}</a>
                                            </li>
                                            <li className="invoiceDetails">
                                                <a>Total</a>
                                                <a className="invoiceData">{"LKR " + total}</a>
                                            </li>

                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col>

                                <h1 >Make your Payment</h1>
                                <Row>
                                    <Col >
                                        <Card className="paymentCard">
                                            <CardBody>
                                                <CardTitle className="paymentCardTitle">Deposit to our Bank account</CardTitle>

                                                <CardText>You can pay extract amount to following bank account</CardText>
                                                <Button className="payCardBtn" onClick={() => this.toggleBankData()}>Show Bank Details</Button>
                                                <Collapse isOpen={this.state.showBankData} tag="h5" className="mt-2">
                                                    <div className="list">
                                                        <li className="invoiceDetails">
                                                            <a>Bank Name:</a>
                                                            <a className="invoiceData">ABC Bank,X Branch</a>
                                                        </li>

                                                        <li className="invoiceDetails">
                                                            <a>Account Number:</a>
                                                            <a className="invoiceData">123456789</a>
                                                        </li>
                                                        <li className="invoiceDetails">
                                                            <a>Account Holder</a>
                                                            <a className="invoiceData">Threadz Lab</a>
                                                        </li>

                                                    </div>
                                                </Collapse>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                    <Col >
                                        <Card className="paymentCard">
                                            <CardBody>
                                                <CardTitle className="paymentCardTitle">Pay Online</CardTitle>

                                                <CardText>Make your payment online</CardText>
                                                <Button className="payCardBtn">Pay Now</Button>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <div className="text-danger">{selectedMethod === "" ||
                            selectedPackage.Pack_Name === "" ||
                            ScheduleData === "" ||
                            (uploadData.AdDesign === "" &&
                                uploadData.designFile === "") ?
                            "There are unfilled sections.Please fill hem out." : ""}</div>
                        <Button
                            disabled={
                                selectedMethod === "" ||
                                    selectedPackage.Pack_Name === "" ||
                                    ScheduleData === "" ||
                                    (uploadData.AdDesign === "" &&
                                        uploadData.designFile === "") ?
                                    true : false
                            }
                            className="btn-next"
                            color="success "
                            size="lg"
                            onClick={() => { this.props.submitCamp(); }}
                        >Finish</Button>
                    </div>
                    : (paymentRoute === "loading") ?
                        <div className="paymentLoading">
                            <Spinner color="primary" className="paymentSpinner"></Spinner>
                            <div className="text-primary h4">Please wait...<br></br>Submitting your Campaign</div>
                        </div>
                        : (paymentRoute === "done") ?
                            <div className="paymentLoading">
                                <FontAwesomeIcon size="7x" icon={"check-circle"} className="text-success animated bounce paymentSpinner"></FontAwesomeIcon>
                                <div className="text-success h4">Done!</div>
                            </div>
                            :
                            <div className="paymentLoading">
                                <FontAwesomeIcon size="7x" icon={"frown"} className="text-danger animated bounce paymentSpinner"></FontAwesomeIcon>
                                <div className="text-danger h4">Opps! Something went wrong<br></br> Try Refreshing the browser</div>
                            </div>




                }

            </div>
        )
    }
}


export default Payment;