import React from "react";
import { Table, Spinner, Badge } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Error from "../loadingError/Error"
class DashboardTable extends React.Component {
    constructor() {
        super();
        this.state = {
            Campaigns: [],
            route: "loading"
        }
    }
    //Sample Return Json
    // {
    //     "Camp_ID": 86,
    //     "Caption": "alclasses",
    //     "design_URL": "peppp",
    //     "UID": "10",
    //     "keywords": "physic",
    //     "audience": "A/l",
    //     "city1": "gamph",
    //     "city2": "kadawatha",
    //     "city3": "nugegoda",
    //     "scheduled_date": "3/8/2019",
    //     "isDesigning": 1,
    //     "payment_amount": "1500.00",
    //     "payment_status": "pen",
    //     "selected_package": "FB2",
    //     "date_created": "2019-05-19 13:30:20"
    // }
    componentWillMount = () => {
        this.loadData();
    }
    loadData=()=>{
        this.setState({ Campaigns: "", route: "loading" })

        fetch(`https://th.threadzlab.com/getAllCampByUID.php?UID=${this.props.uid}`).then(res => res.json())
        .then(data => {
            this.setState({ Campaigns: data, route: "done" })
        }).catch(e=>{
            this.setState({ Campaigns: "", route: "error" })

        })
    }
    render() {
       
        return (
            <div className="containerDashboard  h6">
                {
                    this.state.route === "loading" ?
                        <div>
                            <div className="paymentLoading">
                                <Spinner color="primary" className="paymentSpinner"></Spinner>
                            </div>
                        </div>
                        :(this.state.route==="error"?
                        <Error reloadFunction={this.loadData}></Error>
                        :
                        <div >
                         <h2 className="text-primary">  <FontAwesomeIcon style={{color:"0303D4"}}  size="1x" icon={"chart-line"}></FontAwesomeIcon> Dashboard:your campaigns</h2>
                            
                            <Table  striped>
                                <thead>
                                    <tr>
                                        <th>Campaign ID</th>
                                        <th>Caption</th>
                                        <th>Scheduled Date</th>
                                        <th>Design Request</th>
                                        <th>Date Created</th>
                                        <th>Payment</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.Campaigns.map((camp, i) => {
                                        return (
                                        <tr key={i}>
                                            <th  scope="row">{camp.Camp_ID}</th>
                                            <td >{camp.Caption}</td>
                                            <td >{camp.scheduled_date}</td>
                                            <td >{camp.isDesigning===0?
                                        "No request"  
                                        :"Requested"  
                                        }</td>
                                            <td >{camp.date_created}</td>
                                            <td ><Badge pill color={
                                                camp.payment_status === "done" ? "success" : "warning"}>
                                                {camp.payment_status}
                                            </Badge>
                                            </td>
                                        </tr>
                                        )
                                    })}


                                </tbody>
                            </Table>
                            
                        </div>
                        )
                        
                }

            </div>
        )
    }
}


export default DashboardTable