import React from "react";
import firebase from "firebase";
import { Modal, ModalHeader, ModalBody, ModalFooter, Col, Row, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import "./profile.css"
class Profile extends React.Component {
  constructor() {
    super();
    this.state = {
      userProfile: {
        uid: "",
        name: "",
        email: ""
      },
      authProvider: "",
      ChangedName: "",
      modal: false,
      modalText: ""

    }
    this.toggle = this.toggle.bind(this);
  }

  updateUserProfile = () => {
    let user = firebase.auth().currentUser
    user.updateProfile({
      displayName: this.state.ChangedName

    }).then(() => {
      // Update successful.
      this.toggle("Successfully your name has changed");

      setTimeout(() => {
        this.props.ChangeRoute("front")
      }, 3000);
     // console.log("Done")
    }).catch((error) => {
      // An error happened.
      this.toggle("Something went wrong");
     // console.log(error)
    });
  }

  deleteAccount = () => {


    if (window.confirm("Are you sure you want to delete your Tutors' Hub account? This will delete your all campaign data")) {
      var user = firebase.auth().currentUser;
      user.delete().then(() => {
        // User deleted.
        this.toggle("Your Tutors' Hub account successfully deleted.");
        setTimeout(() => {
          this.props.ChangeRoute("front")
        }, 3000);
      }).catch(error => {
        // An error happened.
        // console.log(error);
        this.toggle("Sorry something went wrong.Try signing-out and signing-in to your account");
      })
    } else {

    }




  }
  OnNameChange = (value) => {
    // console.log(value)
    if (this.state.userProfile.name !== value) {
      this.setState({ ChangedName: value })
    }
  }

  ChangePassword = () => {
    var auth = firebase.auth();
    var emailAddress = this.props.userProfile.email;

    auth.sendPasswordResetEmail(emailAddress).then(() => {
      // Email sent.
      this.props.toastChange("Password reset", "Password reset e-mail sent.Check your inbox", true);
    }).catch((error) => {
      // An error happened.
      // console.log(error);
      this.props.toastChange("Password reset", "Password reset falied.Please contact admin with your email", true);
    });
  }
  toggle(text) {
    this.setState(prevState => ({
      modal: !prevState.modal,
      modalText: text
    }));
  }



  componentWillMount = () => {
    var user = firebase.auth().currentUser;
    //console.log(user)

    if (user) {
      this.setState({
        userProfile: {
          uid: user.uid,
          name: user.displayName,
          email: user.email
        },
        authProvider: user.providerData[0].providerId,

        modal: false
      })

      // console.log(user.providerData)
    } else {
      // No user is signed in.
      this.props.toastChange("Your session timeout", "Something went wrong please try refreshing the browser", true)
    }

  }

  render() {
    const { name, email, uid, photoURL, teleNum } = this.props.userProfile
    return (

      <div className="text-center container">
        <Row>
          <Col >
            <img src={photoURL}
              class="rounded-circle z-depth-2 shadow"
              width="200"
              height="200"
              alt=""></img>
            <h3>{name}</h3>
            <h4>{email}</h4>

            <hr></hr>


          </Col>
        </Row>
        <Row>
          <Col sm="5" className="text-primary font-weight-bold float-left" md={{ size: 4, offset: 2 }}>Your Information</Col>

        </Row>
        <Row>
          <Col sm="12" md={{ size: 6, offset: 3 }}><hr></hr></Col>
        </Row>
        <Row className="p-2">
          <Col sm="12" md={{ size: 6, offset: 3 }}>
            <td className=""><Label for="name">Name</Label></td>


          </Col>
          <Col sm="12" md={{ size: 6, offset: 3 }}>
            <td>
              <Input name="name" placeholder={name} onChange={(e) => { this.OnNameChange(e.target.value) }}></Input>

            </td>
          </Col>
        </Row>
        <Row className="p-2">
          <Col m="12" md={{ size: 6, offset: 3 }}>
            <td ><Label for="name">E-mail</Label></td>
          </Col>
          <Col md={{ size: 6, offset: 3 }}>
            <td>
              <Input name="name" disabled value={email}></Input>
            </td>
          </Col>
        </Row>
        <Row className="p-2">
          <Col m="12" md={{ size: 6, offset: 3 }} >
            <td ><Label for="name"  >Contact Number</Label></td>

          </Col>


          <Col m="12" md={{ size: 6, offset: 3 }}>
            <td>
              <Input name="name" disabled value={teleNum}></Input>
              <div style={{ fontSize: "12px" }}>*When creating campaigns you can give alternative contact number</div>
            </td>
          </Col>
        </Row>

        <Row>
          <Col className="text-left" sm="12" md={{ size: 6, offset: 3 }}>
            <Button
              disabled={
                this.state.ChangedName === "" ?
                  true : false
              }
              onClick={() => {
                this.updateUserProfile();

              }}
              type="button"
              className="cardbutton text-center w-50"
            >Save Changes</Button>
          </Col>
        </Row>
        <br></br>



        <Row>
          <Col sm="5" className="text-primary font-weight-bold float-left" md={{ size: 4, offset: 2 }}>Change Password</Col>
        </Row>
        <Row>
          <Col className="text-left" sm="12" md={{ size: 6, offset: 3 }}><hr></hr>
            <div>{`You can change your password here.Password reset email will sent to ${email}.`}</div>
            <br></br>
            {
              this.state.authProvider === "google.com" ?
                <div style={{ fontSize: "12px" }} className="text-danger">*You can't change password because you have signed in using google account</div>
                :
                ""
            }
            <Button
              disabled={this.state.authProvider === "google.com" ?
                true
                :
                false
              }
              onClick={() => { this.ChangePassword() }}
              type="button"
              className="cardbutton text-center w-50"
            >Change Password</Button>
          </Col>
        </Row>




        <br></br>

        <Row>
          <Col sm="5" className="text-primary font-weight-bold float-left" md={{ size: 4, offset: 2 }}>Account Action</Col>
        </Row>
        <Row>
          <Col className="text-left" sm="12" md={{ size: 6, offset: 3 }}><hr></hr>
            <div>{`${name},If you need to say bye.You can deactivate your tutor's Hub account here`}</div>
            <br></br>
            <Button
              onClick={this.deleteAccount}
              type="button"
              className="cardbutton text-center w-50 bg-danger text-light"
            >Deactivate now</Button>
          </Col>
        </Row>

        <Modal isOpen={this.state.modal} toggle={this.toggle} >
          <ModalHeader >User profile update</ModalHeader>
          <ModalBody>
            {this.state.modalText}
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => { this.toggle("") }}>Close</Button>{' '}

          </ModalFooter>
        </Modal>




      </div>
    )
  }
}


export default Profile;