import React from "react";
import { FormFeedback, Button, Form, FormGroup, Label, Input, Card, CardBody, CardTitle, Spinner } from 'reactstrap';
import Toast from "../toast/Toasts"
import logo from "../logoWhite.png"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import firebase from "firebase"
var Gprovider = new firebase.auth.GoogleAuthProvider();
var FbProvider = new firebase.auth.FacebookAuthProvider();

class Register extends React.Component {
    constructor(props) {
        super();
        this.state = {
            inputPassword: '',
            inputConPassword: '',
            loading: false,
            open: true,

            nameError: "",
            passwordError: "",
            emailError: "",
            conPasswordError: "",
            contactNumError: ""

        }
    }
    validate = () => {
        if (this.state.inputConPassword === this.state.inputPassword) {

        }
    }
    validateName = (value) => {

        if (value.length < 4) {
            this.setState({ nameError: "Name must at least long 3 characters" })
            this.props.onNameChange(value);
        } else {
            this.setState({ nameError: "" })
            this.props.onNameChange(value);
        }


    }
    validatePassword = (value) => {
        if (value.length < 9) {
            this.setState({ passwordError: "Please make sure your password has more than 8 characters" })

        } else {
            this.setState({ passwordError: "", inputPassword: value })
            this.props.onPasswordChange(value);
        }
    }
    validateEmail = (value) => {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)) {
            this.setState({ emailError: "" })
            this.props.onEmailChange(value)
        } else {
            this.setState({ emailError: "Please enter a valid email address" })


        }
    }
    ConfirmPassword = (value) => {
        if (this.state.inputPassword === value) {
            this.setState({ conPasswordError: "" })
        } else {
            this.setState({ conPasswordError: "Passwords are not matching" })
        }
    }

    validateTeleNumber = (value) => {
        var phoneNo = /^\d{10}$/;
        value === "" || !value.match(phoneNo) ?
            this.setState({ contactNumError: "Please enter a valid contact number" })
            :
            this.setState({ contactNumError: "" })
        this.props.onTelChange(value)
    }
    GoogleSIgnIn = () => {
        firebase.auth().signInWithPopup(Gprovider).then((result) => {
            // This gives you a Google Access Token. You can use it to access the Google API.
            //  var token = result.credential.accessToken;
            // The signed-in user info.
            
            var user = result.user;
           
            this.props.changeUserProfile(user.displayName, user.email, user.uid, user.photoURL,user.providerData[0].providerId)
            this.props.toastChange("startcamp");
            //console.log(token);
            // console.log(user);
            // ...
        }).catch( (error) =>{
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            if (errorCode) {
                this.props.toastChange("Google Signin", "Something went wrong", true)
                setTimeout(() => {
                    this.props.toastChange("", "", false)
                }, 5000);

            }
            // console.log(errorCode);
            // ...
        });
    }
    FacebookSignIn = () => {
        firebase.auth().signInWithPopup(FbProvider).then((result) => {
            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            this.props.toastChange(user.displayName, user.email, user.uid, user.photoURL)
            this.props.ChangeRoute("startcamp");
            // console.log(user)
            // ...
        }).catch((error) => {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // console.log(errorCode)

            if (errorCode === "auth/account-exists-with-different-credential") {
                this.props.toastChange("Facebook Login", "Your facebook accout is associated email is already registered.", true)
                setTimeout(() => {
                    this.props.toastChange("", "", false)
                }, 5000);

            }
            // ...
        });
    }
    render() {
        const {
            onEmailChange,
            onNameChange,
            onPasswordChange,
            onTelChange,
            onGenderChange,
            registerWithEmail,
            ChangeRoute,
            toastChange
        } = this.props

        let password = ""
        return (

            <div className={this.state.open === true ? "animated bounceInDown signin" : "animated bounceOutDown signin"}>
                <Card className="mx-auto text-primary  signinCard mt-3 p-3 shadow align-middle">
                    <FontAwesomeIcon icon="arrow-left" size="lg" onClick={() => {
                        this.setState({ open: false }); setTimeout(() => {
                            ChangeRoute("front")
                        }, 500);
                    }}></FontAwesomeIcon>
                    <CardBody>
                        {/* <img width="50%" src={logo}></img> */}
                        <CardTitle className="h3 text-center text-primary font-weight-bold">Create an Account</CardTitle>
                        <div className="">
                                <div className="googleSignin" onClick={this.GoogleSIgnIn} outline color="danger">
                                    <FontAwesomeIcon className="awsomeicon" icon={['fab', 'google']}></FontAwesomeIcon>
                                    Sign in with Google
                                </div>

                                {"  "}


                                <div className="facebookSignin" onClick={this.FacebookSignIn} outline color="primary">
                                    <FontAwesomeIcon size="1.5x" className="awsomeicon" icon={['fab', 'facebook']} fixedWidth></FontAwesomeIcon>
                                    Sign in with Facebook
                                 </div>
                            </div>
                        OR
                        <hr></hr>
                        <Form >

                            <FormGroup>
                                <Label for="exampleEmail" hidden>Name</Label>
                                <Input
                                    invalid={this.state.nameError === "" ? false : true}
                                    className=" text-primary"
                                    onChange={(e) => this.validateName(e.target.value)}
                                    onBlur={(e) => this.validateName(e.target.value)}
                                    type="text"
                                    name="name"
                                    id="name"
                                    required
                                    placeholder="Name" />
                                <FormFeedback>{this.state.nameError}</FormFeedback>
                            </FormGroup>
                            {'  '}
                            <FormGroup>
                                <Label for="exampleEmail" hidden>Password</Label>
                                <Input className=" text-primary"
                                    onChange={(e) => { this.validatePassword(e.target.value) }}
                                    onBlur={(e) => { this.validatePassword(e.target.value) }}
                                    invalid={this.state.passwordError === "" ? false : true}
                                    type="password"
                                    name="password"
                                    id="password"
                                    required
                                    placeholder="Password" />
                                <FormFeedback>{this.state.passwordError}</FormFeedback>
                            </FormGroup>
                            {'  '}
                            <FormGroup>
                                <Label for="conPassword" hidden>Re-Enter Password</Label>
                                <Input className="text-primary"
                                    invalid={this.state.conPasswordError === "" ? false : true}
                                    onBlur={(e) => { this.ConfirmPassword(e.target.value) }}
                                    onChange={(e) => { this.ConfirmPassword(e.target.value) }}
                                    type="password"
                                    name="conPassword"
                                    id="conPassword"
                                    required
                                    placeholder="Re-Enter Password" />
                                <FormFeedback>{this.state.conPasswordError}</FormFeedback>
                            </FormGroup>
                            {'  '}

                            <FormGroup>
                                <Label for="exampleEmail" hidden>E-mail</Label>
                                <Input className="text-primary"
                                    invalid={this.state.emailError === "" ? false : true}
                                    onBlur={(e) => { this.validateEmail(e.target.value) }}
                                    onChange={(e) => { this.validateEmail(e.target.value) }}
                                    type="email"
                                    name="email"
                                    id="exampleEmail"
                                    required
                                    placeholder="E-mail" />
                                <FormFeedback>{this.state.emailError}</FormFeedback>
                            </FormGroup>
                            {'  '}
                            {/* <FormGroup>
                                <Label for="examplePassword" hidden>Mobile Number</Label>
                                <Input className="text-primary"
                                    invalid={this.state.contactNumError === "" ? false : true}
                                    onChange={(e) => { this.validateTeleNumber(e.target.value) }}
                                    onBlur={(e) => { this.validateTeleNumber(e.target.value) }}
                                    type="tel"
                                    name="mobile"
                                    required
                                    id="mobile"
                                    placeholder="Mobile Number" />
                                <FormFeedback>{this.state.contactNumError}</FormFeedback>
                            </FormGroup>

                            {' '} */}




                            <Button className="btn cardbutton"
                                disabled={this.state.nameError === "" &&
                                    this.state.emailError === ""  &&
                                   
                                    this.state.passwordError===""&&
                                    this.state.conPasswordError === ""  
                                    ?
                                   false
                                    :
                                    true
                                }
                                type="button"
                                onClick={(e) => {
                                    this.setState({ loading: true });
                                    registerWithEmail();
                                    setTimeout(() => {
                                        this.setState({ loading: false })
                                    }, 5000);
                                    
                                }
                                }
                            >
                                {this.state.loading === true ? <Spinner size="sm" color="primary" /> : <div></div>} {" "}Register</Button>

                        </Form>
                    </CardBody>
                </Card>


            </div>
        )
    }
}


export default Register;