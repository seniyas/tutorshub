import React from 'react';

// import { Jumbotron, Button ,CardImg} from 'reactstrap';
import { Card, CardTitle, CardText, CardImg, CardImgOverlay } from 'reactstrap';
import { Link, animateScroll as scroll } from "react-scroll";


import back1 from "./img1.webp"
import back2 from "./soft.webp"
import Button from 'reactstrap/lib/Button';
import Signin from "../SIgnin/signin"
import "../Front/front-style.css"
class Front extends React.Component {


  render() {
    const { ChangeRoute } = this.props
    return (
      <div>
        {/* <Jumbotron>
      <CardImg width="100%" src={back1} alt="Card image cap" />


       
        <p className="lead">Promote your tutions with digital marketing.</p>
        <hr className="my-2" />
        
        <p className="lead">
          <Button color="primary">Start Campaign</Button>
        </p>
      </Jumbotron> */}


        <div>
          <Card className="mt-4 frontContainerCard rounded-0 align-middle" inverse>
            <CardImg className="frontimg" width="100%" src={back1} alt="Card__cap" />
            <CardImgOverlay className="">
              <h1 className="animated fronttextoverlayTitle fronttextoverlayTitlesmall fadeInLeft delay-0.5s   mt-5 text-pop-up-top display-3 align-middle frontContainerCard">
                Tutors' HUB</h1>

              <CardText className="h3 fronttextoverlay animated fadeInDown delay-1s">Wanna move to digital marketing?<br></br>Here's the easiest way.
           <br></br>
                <Button
                  onClick={() => ChangeRoute("register")}
                  className="frontbu btn">Get Started</Button>
                {" "}
                <Link
                  activeClass="active"
                  to="card2"
                  spy={true}
                  smooth={true}
                  offset={-70}
                  duration={500}
                ><Button  className="frontbu btn">Learn More</Button></Link>

              </CardText>


            </CardImgOverlay>
          </Card>
          <Card ref="card2" name="card2" className="frontContainerCard2 rounded-0 align-middle" inverse>
            <CardImg className="frontimg2" width="100%" src={back2} alt="Card image cap" />
            <CardImgOverlay className="align-middle">
              <h1 className="animated fronttextoverlayTitle fronttextoverlay2 fronttextoverlayTitlesmall fadeInLeft delay-0.5s   mt-5 text-pop-up-top display-3 align-middle frontContainerCard">
                Tutors' HUB</h1>

              <CardText className="h3 card2Text animated fadeInDown delay-1s">
                Let us handle your marketing campaigns.
              </CardText>

            </CardImgOverlay>
          </Card>
        </div>


      </div>
    );
  }

};

export default Front;