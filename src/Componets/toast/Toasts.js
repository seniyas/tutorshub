import React from "react";
import { Toast, ToastBody, ToastHeader } from 'reactstrap';
import "./Toasts.css";
class Toasts extends React.Component {
    constructor() {
        super();
    }

    render() {
        const {title,body,isOpen}=this.props
        return (
            <div  className=" p-3  tost  my-2 rounded bg-docs-transparent-grid">
                <Toast className="animated fadeInLeft bg-light" isOpen={isOpen} >
                    <ToastHeader className="bg-light text-primary" >
                       {title}
                    </ToastHeader>
                    <ToastBody className="bg-light text-dark">
                        {body}
                    </ToastBody>
                </Toast>
            </div>
        )
    }
}


export default Toasts;