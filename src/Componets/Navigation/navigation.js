import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,

} from 'reactstrap';
import logo from "./tutorhubLogo.png"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import "./navigation.css"
export default class Example extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }


  render() {
    const { route, ChangeRoute, signOut, userData } = this.props
    return (
      <div >
        <Navbar fixed="top" color="light" className="navbar navbar-light  shadow bg-primary " expand="md">

          <NavbarBrand className="text-primary" href="/">

            <img src={logo} width="80" height="40" alt=""></img>

          </NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>

              {route === "startcamp" || route === "profile" || route === "dashboard" ?
                (route === "startcamp" ?
                  <NavItem className=" navigationSelected  ">
                    <NavLink href="#" onClick={() => ChangeRoute("startcamp")} className=" text-primary" >
                      <FontAwesomeIcon className="m-1" icon={"magic"}></FontAwesomeIcon>
                      Start Campaign

                  </NavLink>
                  </NavItem> :
                  <NavItem className="navigation">
                    <NavLink href="#" onClick={() => ChangeRoute("startcamp")} className=" text-primary" >
                      <FontAwesomeIcon className="m-1" icon={"magic"}></FontAwesomeIcon>

                      Start Campaign

                  </NavLink>
                  </NavItem>)



                : ""




              }
              {
                route === "startcamp" || route === "profile" || route === "dashboard" ?
                  (route === "dashboard" ?
                    <NavItem className="navigationSelected ">
                      <NavLink href="#" onClick={() => ChangeRoute("dashboard")} className="text-primary align-middle">
                        <FontAwesomeIcon className="navicon" icon={"chart-line"}></FontAwesomeIcon>

                        Dashboard
                     </NavLink>
                    </NavItem>
                    :
                    <NavItem className="navigation">
                      <NavLink href="#" onClick={() => ChangeRoute("dashboard")} className="text-primary align-middle">
                        <FontAwesomeIcon className="navicon" icon={"chart-line"}></FontAwesomeIcon>

                        Dashboard
                      </NavLink>
                    </NavItem>
                  )
                  : ""


              }

              {route === "startcamp" || route === "profile" || route === "dashboard" ?
                <NavItem>
                  <UncontrolledDropdown className="text-primary bg-light  " nav inNavbar>
                    <DropdownToggle className="text-primary profileDropDown " nav caret>
                      <img src={userData.photoURL} class="rounded-circle z-depth-2" width="25" height="25" alt=""></img>
                      {` ${userData.name}`}
                    </DropdownToggle>
                    <DropdownMenu className="dropdownMenu text-light bg-light " right>
                      <DropdownItem className="round p-2 text-primary bg-light" onClick={() => ChangeRoute("profile")} >
                        <FontAwesomeIcon icon="user-circle" size="lg"></FontAwesomeIcon>
                        <a className="p-2">Profile</a>
                      </DropdownItem>

                      <DropdownItem onClick={() => { signOut() }} className="round p-2 text-primary bg-light " >
                        <FontAwesomeIcon icon="sign-out-alt" size="lg"></FontAwesomeIcon>
                        <a className="p-2">Log Out</a>
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </NavItem>
                : (route === "signin") ?
                  <NavLink href="#" className="registerbtn" onClick={() => { ChangeRoute("register") }} >Register</NavLink>
                  :
                  <div style={{display:"flex"}}>
                    <NavItem>
                      <NavLink href="#" className="registerbtn" onClick={() => { ChangeRoute("register") }} >Register</NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink href="#" className="signinbtn" onClick={() => { ChangeRoute("signin") }}>Sign In</NavLink>
                    </NavItem>
                  </div>

              }


            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}