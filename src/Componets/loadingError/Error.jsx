import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {Button}from "reactstrap"
import "./error.css"
class Error extends React.Component {
    constructor() {
        super();

    }

    render() {
        const {reloadFunction}=this.props
        return (
            <div className="paymentLoading ">
                <FontAwesomeIcon size="7x" icon={"frown"} className="text-danger animated bounce paymentSpinner"></FontAwesomeIcon>
                <div className="text-danger h4">Opps! Something went wrong<br></br> Try Refreshing the browser<br></br>
              
                </div>
                <div>
                     <Button onClick={()=>reloadFunction()} className="errorbtn">Re-try</Button>
                </div>
                 
            </div>
        )
    }
}

export default Error;