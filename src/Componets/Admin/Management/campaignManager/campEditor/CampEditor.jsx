import React from 'react';
import {Row,Col,Input, Button, Modal, ModalHeader, ModalBody, ModalFooter,CustomInput, Form, FormGroup, Label } from 'reactstrap';

class CampEditor extends React.Component {
    constructor() {
        super();

    }



    render() {
        const { toggleEditor, editor, SCamp } = this.props
        //   const{Camp_ID,Caption,design_URL,UID,keywords,audience,city1,city2,city3,scheduled_date,isDesigning,payment_amount,payment_status,selected_package,date_created}=selectedCamp;
        const closeBtn = <button className="close" onClick={toggleEditor}></button>;
        console.log(SCamp);
        return (
            <div>


                <Modal size="lg" isOpen={editor} toggle={toggleEditor} >

                    <ModalHeader toggle={() => toggleEditor()} close={closeBtn}>Edit Campaign</ModalHeader>
                    <ModalBody>
                    <Form>
        <Row form>
          <Col md={6}>
            <FormGroup>
              <Label for="exampleEmail">Caption</Label>
              <Input type="text" name="caption" id="caption" placeholder={SCamp.Caption} />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="examplePassword">Audience</Label>
              <Input type="text" name="audience" id="audience" placeholder={SCamp.audience} />
            </FormGroup>
          </Col>
        </Row>
        <FormGroup>
          <Label for="city1">Target City 1</Label>
          <Input type="text" name="city1" id="city1" placeholder={SCamp.city1}/>
        </FormGroup>
        <FormGroup>
        <Label for="city2">Target City 2</Label>
          <Input type="text" name="city2" id="city2" placeholder={SCamp.city2}/>
        </FormGroup>
        <FormGroup>
        <Label for="exampleAddress">Target City 3</Label>
          <Input type="text" name="city3" id="city3" placeholder={SCamp.city3}/>
        </FormGroup>
        <Row form>
          <Col md={6}>
            <FormGroup>
            <Label for="exampleAddress">Ad Design URL</Label>
          <Input type="text" name="designUrl" id="designUrl" placeholder={SCamp.design_URL}/>
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup>
              {/* <Label for="isDesigning">State</Label> */}
              <CustomInput type="checkbox" id="isDesigning" label="Request to design" />
            </FormGroup>
          </Col>
          <Col md={2}>
            <FormGroup>
              <Label for="keywords">KeyWords</Label>
              <Input type="text" name="keywords" id="keywords" placeholder={SCamp.isDesign}/>
            </FormGroup> 
          </Col>
        </Row>
        <FormGroup>
          <Label for="city1">Selected Package</Label>
          <Input type="text" name="pack" id="pack" placeholder={SCamp.selected_package}/>
        </FormGroup>
        <FormGroup>
        <Label for="city2">Payment total amount</Label>
          <Input type="text" name="paymentAmount" id="paymentAmount" placeholder={SCamp.payment_amount}/>
        </FormGroup>
        <FormGroup>
        <Label for="exampleAddress">Payment status</Label>
        <CustomInput type="checkbox" id="paymentStatus" label={SCamp.payment_status} />

        </FormGroup>
      
      
      </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={() => toggleEditor()}>Do Something</Button>{' '}
                        <Button color="secondary" onClick={() => toggleEditor()}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default CampEditor;