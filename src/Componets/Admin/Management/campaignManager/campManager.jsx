import React from "react";
import { Table, Button, Input, Row, Col, FormGroup, Label } from 'reactstrap';
import "./campManager-style.css"
import CampEditor from "./campEditor/CampEditor"
let OriginalCamps = [];
class CampManager extends React.Component {
    constructor() {
        super();
        this.state = {
            editor: false,
            SCamp: {},


            camps: [
                {
                    Camp_ID: 141,
                    Caption: "SOmething            ",
                    UID: "NWud7yCyefONMRtiFCTv6HJiekQ2            ",
                    audience: "3         ",
                    city1: "3         ",
                    city2: "3         ",
                    city3: "4         ",
                    date_created: "2019-06-02 11:35:16",
                    design_URL: "eee            ",
                    isDesigning: 0,
                    keywords: "Students,biology            ",
                    payment_amount: "0.00",
                    payment_status: "pending   ",
                    scheduled_date: "2019-06-03",
                    selected_package: "4,2       ",
                }
            ],
            filterMethod: "",
            filterText: "",
            dateFrom: "",
            dateTill: "",
            date: ""
        }
    }
    componentWillMount() {
        this.getLatestCamps("10");
    }

    toggleEditor = (i) => {
        this.setState(prevState => ({
            editor: !prevState.editor,
            // SCamp: this.state.camps[i]
        }));
    }

    changeCamps = (camps) => {
        this.setState({
            camps: camps
        })
    }
    getLatestCamps = (limit) => {
        fetch(`http://th.threadzlab.com/getCampaigns.php?filterMethod=latest&limit=${limit}`)
            .then(res => res.json())
            .then(data => {
                // console.log(data);
                this.changeCamps(data);
                OriginalCamps = data;
            })
    }
    submitFilter = () => {
        if (this.state.filterMethod === "dateRange") {
            var url = `http://th.threadzlab.com/getCampaigns.php?filterMethod=dateRange&fromDate=${this.state.dateFrom}&tillDate=${this.state.dateTill}`
            console.log(url);
            fetch(url)
                .then(res => res.json())
                .then(data => {
                    this.changeCamps(data)
                    OriginalCamps = data;
                })
        } else {
            var url = `http://th.threadzlab.com/getCampaigns.php?filterMethod=${this.state.filterMethod}&${this.state.filterMethod === "id" ? "campID" : (this.state.filterMethod === "uid") ? "UID" : (this.state.filterMethod === "cap") ? "caption" : "date"}=${this.state.filterText}`
            console.log(url);
            fetch(url)
                .then(res => res.json())
                .then(data => {
                    this.changeCamps(data)
                    OriginalCamps = data;
                })
        }

    }
    searchName = (searchTxt) => {
        let result = OriginalCamps.filter(camp => {
            return camp.Caption.toLowerCase().includes(searchTxt.toLowerCase())
        })

        this.changeCamps(result);
    }
    render() {
        return (
            <div className="campContainer">
                <h1>Campaign Manager</h1>
                <Row form>
                    <Col md={3}>
                        <FormGroup>
                            <label htmlFor="">Get Campaigns by:</label>
                            <Input type="select" name="select"
                                onChange={(e) => { this.setState({ filterMethod: e.target.value }) }}
                                id="exampleSelect">
                                <option value="dateRange">Date range</option>
                                <option value="date">Specific date</option>
                                <option value="id">Campaign ID</option>
                                <option value="uid">User ID</option>
                                <option value="cap">Caption</option>

                            </Input>
                        </FormGroup>
                    </Col>
                    <Col md={6}>
                        <FormGroup>
                            {
                                this.state.filterMethod === "dateRange" ?
                                    <Row>
                                        <Col>
                                            <label htmlFor="fromDate">From:</label>
                                            <Input onChange={(e) => this.setState({ dateFrom: e.target.value })}
                                                type="date"
                                                name="fromDate"
                                                id="fromDate" />
                                        </Col>
                                        <Col>
                                            <label htmlFor="tillDate">To:</label>
                                            <Input onChange={(e) => this.setState({ dateTill: e.target.value })}
                                                type="date"
                                                name="tillDate"
                                                id="tillDate" />
                                        </Col>
                                    </Row>
                                    : (this.state.filterMethod === "date") ?
                                        <Col>
                                            <label htmlFor="tillDate">To:</label>
                                            <Input onChange={(e) => this.setState({ filterText: e.target.value })}
                                                type="date"
                                                name="sDate"
                                                id="sDate" />

                                        </Col>
                                        :
                                        <Row>
                                            <Col>
                                                <label>Filter text</label>
                                                <Input onChange={(e) => this.setState({ filterText: e.target.value })}
                                                    type="text"></Input>
                                            </Col>
                                        </Row>
                            }
                        </FormGroup>
                    </Col>
                    <Col md={2} >
                        <Row>
                            <Col>
                                <label>Get</label>
                                <Button onClick={() => this.submitFilter()}>Get Campaigns</Button>
                            </Col>
                        </Row>
                    </Col>

                </Row>

                <Row>
                    <Input type="text" onChange={(e) => this.searchName(e.target.value)}></Input>
                </Row>
                {<label>{this.state.camps.length + " results"}</label>}
                <Table className="table" responsive striped>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>User ID</th>
                            <th>Caption</th>
                            <th>Scheduled date</th>
                            <th>Audience</th>
                            <th>Date created</th>
                            <th>Design request</th>
                            <th>Payment amount</th>
                            <th>Payment status</th>
                            <th>Actions</th>




                        </tr>
                    </thead>
                    <tbody>


                        {
                            this.state.camps.map((camp, i) => {
                                return (
                                    <tr>
                                        <td>{camp.Camp_ID}</td>
                                        <td>{camp.UID}</td>
                                        <td>{camp.Caption}</td>
                                        <td>{camp.scheduled_date}</td>
                                        <td>{camp.audience}</td>
                                        <td>{camp.date_created}</td>
                                        <td>{camp.isDesigning}</td>
                                        <td>{camp.payment_amount}</td>
                                        <td>{camp.payment_status}</td>
                                        <td><Button onClick={() => {this.setState({SCamp:camp});
                                     this.toggleEditor()}}>Edit</Button></td>
                                    </tr>)
                            })
                        }




                    </tbody>
                </Table>
                <CampEditor
                    SCamp={this.state.SCamp}
                    editor={this.state.editor}
                    toggleEditor={this.toggleEditor} ></CampEditor>
            </div>
        )
    }
}


export default CampManager;