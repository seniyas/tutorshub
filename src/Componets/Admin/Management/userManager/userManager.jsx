import React from "react";
import { Table, Button,Input } from 'reactstrap';
import "./userManager-style.css";
let UsersOriginal=[];
class UserManager extends React.Component {
    constructor() {
        super();
        this.state = {
            users: [
                {
                    uid: "HfpS2ycNGRPcDjy10iq4xLF4Hd53",
                    email: "indikadissanayake99@gmail.com",
                    emailVerified: true,
                    displayName: "Indi",
                    photoURL: "http://icons.iconarchive.com/icons/paomedia/small-n-flat/512/user-male-icon.png",
                    disabled: false,
                    metadata: {
                        lastSignInTime: "Fri, 31 May 2019 14:01:59 GMT",
                        creationTime: "Thu, 30 May 2019 15:33:45 GMT"
                    },
                    passwordHash: "ulNqf7dKR_FDyCQz-07u-KWFGNLEQZuSc6lSCHP7BoXUBahBZ_FtJF2pSr_xkIUIMT1pOrd-MjfMge1kMXroSA==",
                    passwordSalt: "RQTfUtBNaFcKdA==",
                    tokensValidAfterTime: "Thu, 30 May 2019 15:33:45 GMT",
                    providerData: [
                        {
                            uid: "indikadissanayake99@gmail.com",
                            displayName: "Indi",
                            email: "indikadissanayake99@gmail.com",
                            photoURL: "http://icons.iconarchive.com/icons/paomedia/small-n-flat/512/user-male-icon.png",
                            providerId: "password"
                        }
                    ]
                },
            ],
        }
    }
    loadUsers = () => {
        fetch("https://us-central1-tutorshub-e429a.cloudfunctions.net/api")
            .then(res => res.json())
            .then(data => {
                this.changeUsers(data);
                UsersOriginal=data;
            })
    }

    changeUsers=(Users)=>{
        this.setState({users:Users});
    }
    searchName = (searchTxt) => {
        let result= UsersOriginal.filter(user=>{
          return user.displayName.toLowerCase().includes(searchTxt)
        })
        this.changeUsers(result);
    }

    deleteUser = (id) => {
        fetch(`https://us-central1-tutorshub-e429a.cloudfunctions.net/deleteUser?id=${id}`)
            .then(res => res.json()).then(data => {
                data === "done" ?
                    this.loadUsers()
                    :
                    alert(`Attempt to delete ${id} failed!`)
            })
    }
    componentWillMount() {
        this.loadUsers();
    }
    render() {
        return (
            <div className="userContainer" >
                <h1>User Manager</h1>
                <Input type="text" onChange={(e)=>this.searchName(e.target.value)}></Input>
                <Table className="table" responsive striped>
                    <thead>
                        <tr>
                            <th>UID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Auth Provider</th>
                            <th>Verified</th>
                            <th>Last signed in</th>
                            <th>Actions</th>


                        </tr>
                    </thead>
                    <tbody>


                        {
                            this.state.users.map((user, i) => {
                                return (<tr>
                                    <td>{user.uid}</td>
                                    <td>{user.displayName}</td>
                                    <td>{user.email}</td>
                                    <td>{user.providerData[0].providerId}</td>
                                    <td>{user.emailVerified}</td>
                                    <td>{user.metadata.lastSignInTime}</td>
                                    <td>
                                        <Button onClick={() => this.deleteUser(user.uid)}>Delete</Button>
                                    </td>

                                </tr>)
                            })
                        }




                    </tbody>
                </Table>

            </div>
        )
    }
}


export default UserManager;