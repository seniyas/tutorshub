import React from "react";

import UserManager from "./Management/userManager/userManager";
import CampManager from "./Management/campaignManager/campManager"


class Admin extends React.Component{
   constructor(){
       super();
   }
   
    render(){
        return(
            <div>
                <UserManager></UserManager>
                <CampManager></CampManager>
            </div>
        )
    }
}


export default Admin;