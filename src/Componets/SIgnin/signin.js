import React from 'react';
import { Spinner, Modal, ModalHeader, ModalBody, ModalFooter, Button, Form, FormGroup, Label, Input, Card, CardBody, CardTitle, FormFeedback } from 'reactstrap';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Redirect,
    withRouter
  } from "react-router-dom";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import logo from "../logoWhite.png"
import firebase from "firebase"
import  "./buttons.css"
var Gprovider = new firebase.auth.GoogleAuthProvider();
var FbProvider = new firebase.auth.FacebookAuthProvider();

 
export default class Signin extends React.Component {

    constructor(props) {
        super();
        this.state = {
            open: true,
            email: "",
            password: "",
            modal: false,
            loading: false,
          
        
        }
    }
    resetForm = () => {
        this.setState({ email: "", password: "" });

    }
    toggleModal = () => {
        this.setState({ modal: !this.state.modal })
    }
    GoogleSIgnIn = () => {
        firebase.auth().signInWithPopup(Gprovider).then((result) => {
            // This gives you a Google Access Token. You can use it to access the Google API.
            //  var token = result.credential.accessToken;
            // The signed-in user info.
            
            var user = result.user;
           
            this.props.changeUserProfile(user.displayName, user.email, user.uid, user.photoURL,user.providerData[0].providerId)
            this.props.ChangeRoute("startcamp");
            //console.log(token);
            // console.log(user);
            // ...
        }).catch( (error) =>{
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            if (errorCode) {
                this.props.ChageToastData("Google Signin", "Something went wrong", true)
                setTimeout(() => {
                    this.props.ChageToastData("", "", false)
                }, 5000);

            }
            // console.log(errorCode);
            // ...
        });
    }
    FacebookSignIn = () => {
        firebase.auth().signInWithPopup(FbProvider).then((result) => {
            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            this.props.changeUserProfile(user.displayName, user.email, user.uid, user.photoURL)
            this.props.ChangeRoute("startcamp");
            // console.log(user)
            // ...
        }).catch((error) => {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // console.log(errorCode)

            if (errorCode === "auth/account-exists-with-different-credential") {
                this.props.ChageToastData("Facebook Login", "Your facebook accout is associated email is already registered.", true)
                setTimeout(() => {
                    this.props.ChageToastData("", "", false)
                }, 5000);

            }
            // ...
        });
    }
    SendResetEmail = () => {

        const email = this.state.email;
        var auth = firebase.auth();
        if (email !== "") {
            auth.sendPasswordResetEmail(email).then(() => {
                // Email sent.
                // this.props.onEmailChange()
                this.props.ChageToastData("Password Reset", "Password reset email sent to your email", true)
                setTimeout(() => {
                    this.props.ChageToastData("", "", false)

                }, 5000);
            }).catch((error) => {
                // console.log(error);
                this.props.ChageToastData("Password Reset", "Something went wrong", true)
                setTimeout(() => {
                    this.props.ChageToastData("", "", false)
                }, 5000);
            });
        } else {
            this.props.ChageToastData("Password Reset", "Please enter your email to reset password", true)
            setTimeout(() => {
                this.props.ChageToastData("", "", false)

            }, 5000);
        }
        

    }

   
    render() {
        const { onEmailChange, onPasswordChange, signInwithEmail, ChangeRoute, signInError } = this.props;
        const cardStyle = { cursor: "pointer" }
        return (
            <div className={this.state.open === true ? " animated bounceInDown signin" : "animated bounceOutDown signin"} >

                <Card className="mx-auto text-primary signinCard mt-4 p-3 shadow align-middle" >
                    <FontAwesomeIcon style={cardStyle} onClick={() => {
                        this.setState({ open: false }); setTimeout(() => {
                            ChangeRoute("front")
                        }, 500);
                    }} icon="arrow-left" size="lg" ></FontAwesomeIcon>
                    <CardBody  >
                        <img width="50%" src={logo}></img>
                        <CardTitle className="h3 text-center font-weight-bold">Sign In</CardTitle>
                        <Form >

                            <div className={signInError === "" ? "" : "animated shake h5 text-danger"}>{signInError}</div>

                            <FormGroup>
                                <Label for="exampleEmail" hidden>Email</Label>
                                <Input className=" text-primary"

                                    onChange={(e) => {
                                        onEmailChange(e.target.value);
                                        this.setState({ email: e.target.value })
                                    }}
                                    value={this.state.email}
                                    type="email"
                                    name="email"
                                    id="exampleEmail"
                                    placeholder="Email" />
                            </FormGroup>
                            {'  '}
                            <FormGroup>
                                <Label for="examplePassword" hidden>Password</Label>
                                <Input className=" text-primary"
                                    onChange={(e) => {
                                        onPasswordChange(e.target.value);
                                        this.setState({ password: e.target.value })

                                    }}
                                    value={this.state.password}
                                    type="password"
                                    name="password"
                                    id="examplePassword"
                                    placeholder="Password" />
                            </FormGroup>

                            <a className=" m-2" onClick={() =>this.toggleModal() } >Forget your password?</a>
                            {' '}
                            <br></br>
                            <Button className="btn cardbutton w-25 btn-outline-primary"
                                type="button"
                                onClick={(e) => {
                                    this.setState({ loading: true });
                                    signInwithEmail();
                                    this.resetForm()
                                    setTimeout(() => {
                                        this.setState({ loading: false })
                                    }, 5000);
                                }}

                            >
                                {this.state.loading === true ? <Spinner size="sm"></Spinner> : ""}
                                Sign In</Button>

                            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                                <ModalHeader toggle={this.toggle}>User Password Reset</ModalHeader>
                                <ModalBody>
                                    {
                                        `Click "Reset password" to send password reset email to ${this.state.email}`
                                    }
          </ModalBody>
                                <ModalFooter>
                                    <Button color="primary" onClick={this.SendResetEmail}>Reset Password</Button>{' '}
                                    <Button color="secondary" onClick={this.toggleModal}>Cancel</Button>
                                </ModalFooter>
                            </Modal>
                            <hr></hr>
                            <div className="buttonContainer">
                                <div className="googleSignin" onClick={this.GoogleSIgnIn} outline color="danger">
                                    <FontAwesomeIcon className="awsomeicon" icon={['fab', 'google']}></FontAwesomeIcon>
                                    Sign in with Google
                                </div>

                                {"  "}


                                <div className="facebookSignin" onClick={this.FacebookSignIn} outline color="primary">
                                    <FontAwesomeIcon size="1.5x" className="awsomeicon" icon={['fab', 'facebook']} fixedWidth></FontAwesomeIcon>
                                    Sign in with Facebook
                                 </div>
                            </div>
                        </Form>
                    </CardBody>
                </Card>

            </div>

        );
    }
}