// Import FirebaseAuth and firebase.
import React from 'react';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';


class SignInScreen extends React.Component {
  render() {
    const {firebaseAuth,uiConfig}=this.props;
    return (
      <div>
        <h1>My App</h1>
        <p>Please sign-in:</p>
        <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebaseAuth}/>
      </div>
    );
  }
}

export default SignInScreen;