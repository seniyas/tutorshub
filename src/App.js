import React, { Component } from 'react';
import firebase from "firebase"
import Navigation from "./Componets/Navigation/navigation"
import {Modal,ModalBody,ModalFooter,ModalHeader,Button} from "reactstrap"
import './App.css';
import Front from "./Componets/Front/frontpage"
// import SignInScreen from "./Componets/SIgnin/firebaseUi"
import Signin from './Componets/SIgnin/signin';
import Register from './Componets/Register/Register';
import Toasts from "./Componets/toast/Toasts";
import StartCamp from './Componets/StartCamp/StartCamp';
import Profile from "./Componets/Profile/Profile"
import Dashboard from "./Componets/Dashboard/dashboardTable"
import Admin from "./Componets/Admin/Admin"
import { fab } from "@fortawesome/free-brands-svg-icons"
import { library } from '@fortawesome/fontawesome-svg-core'
import {faMagic, faMobileAlt,faDownload,faThumbsUp,faTag,faStopwatch,faRunning,faUsers,faChartLine,faCloudMoon,faSun,faChevronDown,faFrown,faCheckCircle, faSignOutAlt, faStroopwafel, faCoffee, faHandPointer, faBoxOpen, faUpload, faClock, faMoneyCheckAlt, faArrowLeft, faUserCircle } from '@fortawesome/free-solid-svg-icons'
library.add(faMagic,faMobileAlt,faDownload,faThumbsUp,faTag,faStopwatch,faRunning,faUsers,faChartLine,faCloudMoon,faSun,faChevronDown,faFrown,faCheckCircle, faSignOutAlt, faStroopwafel, faCoffee, fab, faHandPointer, faBoxOpen, faUpload, faClock, faMoneyCheckAlt, faArrowLeft, faUserCircle)


var config = {
  apiKey: "AIzaSyA3FZ2MurHtspVK_Y0Sr64MY_kxpM1i59I",
  authDomain: "tutorshub-e429a.firebaseapp.com",
  databaseURL: "https://tutorshub-e429a.firebaseio.com",
  projectId: "tutorshub-e429a",
  storageBucket: "tutorshub-e429a.appspot.com",
  messagingSenderId: "927357531277"
};
firebase.initializeApp(config);



class App extends Component {



  constructor() {
    super();
    this.state = {
      userProfile: {
        name: "",
        email: "",
        uid: "",
        photoURL: "",
        AuthproviderID: ""
      },


      route: "admin",
      signInError: "",
      signinInputEmail: "",
      signinInputPassword: "",

      toast: {
        title: "Hi",
        body: "Hellow",
        open: false
      },
      registerEmail: "",
      registerPassword: "",
      registerName: "",
      registerTel: "",
      registerGender: "",
      modal:{
        topic:"",
        text:"",
        show:false
      }



    }


  }
componentWillMount=()=>{
  firebase.auth().onAuthStateChanged((user)=>{
    if (user) {
      // User is signed in.
      this.setState({
         userProfile: {
        name:user.displayName,
        email: user.email,
        uid: user.uid,
        photoURL: user.photoURL,
        AuthproviderID: user .providerId
      },})

      this.ChangeRoute("startcamp")
    } else {
      // No user is signed in.
      console.log("no user")
    }
  });
  
}
  uploadImage = (campCaption, file) => {
    console.log("Upload I=mage Run", file);
    let sotrageRef = firebase.storage().ref().child(`Seniya/${file.name}`)
    sotrageRef.put(file).then((snapshot) => {
      return "Done"

    })
  }
  ChageToastData = (title, body, isOpen) => {
    this.setState({
      toast: {
        title: title,
        body: body,
        open: isOpen
      }
    })

    setTimeout(() => {
      this.setState({
        toast: {
          title: "",
          body: "",
          open: false

        }
      })
    }, 5000);
  }

  onRegisterEmailChange = (value) => {
    this.setState({ registerEmail: value })
  }
  onRegisterPasswordChange = (value) => {
    this.setState({ registerPassword: value })
  }
  onRegisterNameChange = (value) => {
    this.setState({ registerName: value })
  }
  onRegisterTelChange = (value) => {
    this.setState({ registerTel: value })
  }
  onRegisterGenderChange = (e) => {
    this.setState({ registerGender: e.target.value })
  }
  onEmailChange = (value) => {
    this.setState({ signinInputEmail: value })
  }
  onPasswordChange = (value) => {
    this.setState({ signinInputPassword: value })
  }
  verifyEmail = () => {
    var user = firebase.auth().currentUser;

    user.sendEmailVerification().then(() => {
      // Email sent.
      //console.log("sent")
      this.ChageToastData("Registration Success!",
        "To continue you need to verify your email.Verification e-mail sent to"+this.state.registerEmail+".Please check your inbox.", true)

      this.ChangeRoute("signin")
    }).catch((error)=>{
      // An error happened.
      // console.log(error)
      this.ChageToastData("Registration Failed!",
      "Something went wrong here,Try using different E-mail", true)
    });
  }
  getUserprofile = (uid) => {

    fetch(`https://th.threadzlab.com/getUserData.php?UID=${uid}`)
      .then(res => res.json())
      .then(data => {
        console.log(data)
        this.setState({
          userProfile: {

            name: data.Name,
            email: data.Email,
            uid: data.UID,
            teleNum: data.MobileNo
          },
          signinInputEmail: "",
          signinInputPassword: ""
        })
      })

  }

  changeUserProfile = (name, email, uid, photoURL, providerId) => {
    this.setState({
      userProfile: {
        name: name,
        email: email,
        uid: uid,
        photoURL: photoURL,
        AuthproviderID: providerId
      }
    })
  }
  registerWithEmail = (email = this.state.registerEmail, password = this.state.registerPassword) => {

    firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(data => {
        var uid = data.user.uid

        data.user.updateProfile(
          {
            displayName: this.state.registerName,
            photoURL: "http://icons.iconarchive.com/icons/paomedia/small-n-flat/512/user-male-icon.png"
          }).then(() => {

          })

        //console.log(uid, this.state.registerName, this.state.registerEmail, this.state.registerTel)
        // fetch(`http://th.threadzlab.com/insertUser.php?
        // UID=${uid}
        // &Name=${this.state.registerName}
        // &Email=${this.state.registerEmail}
        // &MobileNo=${this.state.registerTel}`
        // ).then(res =>
        //   res.json()
        // ).then(data => {
        //   console.log(data)
        // })

      })
      .then(() => {
        this.verifyEmail()

      })
      .catch((error)=>{
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorMessage," ",errorCode);
        
        this.ChageToastData("Registration error!", "Something went wrong please try refreshing the page", true)

      });
  }

  signInwithEmail = (email = this.state.signinInputEmail, password = this.state.signinInputPassword) => {
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then((data) => {
        console.log(data)
        var uid = data.user.uid;
        // var uid = "Xd5hElLno20rBejslX2"
        if (data.user.emailVerified === true) {
          //this.getUserprofile(uid);
          this.setState({
            userProfile: {

              name: data.user.displayName,
              email: data.user.email,
              uid: data.user.uid,
              photoURL: data.user.photoURL

            }
          })
          this.ChangeRoute("startcamp")

        } else {
          // this.verifyEmail();
        
          this.ChageToastData("Log in Failed!", "Your email is not a verified one please check your inbox", true)
        }

      }).catch((error) => {

        var errorCode = error.code;
        var errorMessage = error.message;
        errorCode === "auth/wrong-password" ?
          this.setState({ signInError: "Incorrect password" })
          : errorCode === "auth/user-not-found" ?
            this.setState({ signInError: "User not found,New to here?" })
            : errorCode === "auth/invalid-email" ?
              this.setState({ signInError: "Invalid E-mail" })
              :
              this.setState({ signInError: "Something went wrong,Refresh your browser" })



        console.log(errorCode);
      });
  }
  signOut = () => {
    firebase.auth().signOut().then(() => {
      this.ChangeRoute("front");
    }).catch((error) => {
      console.log(error);
    });
  }
  ChangeRoute = (route) => {
    this.setState({
      route: route
    })
  }
  toggleModal(title,text,show) {
    this.setState(() => ({
      modal:{
        title:title,
        text:text,
        show:show
      }
    }));
  }
  render() {
    return (
      <div>
        <Navigation
          userData={this.state.userProfile}
          ChangeSigninRoute={this.ChangeSigninRoute}
          route={this.state.route}
          ChangeRoute={this.ChangeRoute}
          signOut={this.signOut}
        ></Navigation>

        {this.state.route === "front" ?
          <div>
            <Front
            ChangeRoute={this.ChangeRoute}
            ></Front>


          </div>
          : (this.state.route === "signin") ?
            <div>
              <Front

              ChangeRoute={this.ChangeRoute}
              ></Front>
              <Signin
                ChangeRoute={this.ChangeRoute}
                onEmailChange={this.onEmailChange}
                onPasswordChange={this.onPasswordChange}
                signInwithEmail={this.signInwithEmail}
                changeUserProfile={this.changeUserProfile}
                signInError={this.state.signInError}
                ChageToastData={this.ChageToastData}
              >
              </Signin>
            </div>
            :
            (this.state.route === "home") ?
              <div></div>
              : (this.state.route === "register") ?
                <div>
                  <Front
                  ChangeRoute={this.ChangeRoute}
                  ></Front>
                  <Register
                    
                    onEmailChange={this.onRegisterEmailChange}
                    onPasswordChange={this.onRegisterPasswordChange}
                    onNameChange={this.onRegisterNameChange}
                    onTelChange={this.onRegisterTelChange}
                    onGenderChange={this.onRegisterGenderChange}
                    verifyEmail={this.verifyEmail}
                    registerWithEmail={this.registerWithEmail}
                    ChangeRoute={this.ChangeRoute}
                    toastChange={this.ChageToastData}
                  ></Register>
                </div>
                : (this.state.route === "startcamp") ?
                  <div className="h-100">
                    {/* <Methods></Methods> */}
                    <StartCamp
                      uid={this.state.userProfile.uid}
                      uploadImage={this.uploadImage}
                      toastChange={this.ChageToastData}
                      ChangeRoute={this.ChangeRoute}
                    ></StartCamp>
                  </div>
                  : (this.state.route === "profile") ?
                    <div>
                      <Profile
                        toastChange={this.ChageToastData}
                        userProfile={this.state.userProfile} 
                        ChangeRoute={this.ChangeRoute}
                        ></Profile>
                    </div>
                    :(this.state.route==="dashboard")?
                    <Dashboard uid={this.state.userProfile.uid}></Dashboard>
                    :
                    <Admin></Admin>
        }



        {/* <SignInScreen></SignInScreen> */}
        <Toasts
          title={this.state.toast.title}
          body={this.state.toast.body}
          isOpen={this.state.toast.open}
        ></Toasts>

<Modal isOpen={this.state.modal.show}  >
          <ModalHeader >{this.state.modal.topic}</ModalHeader>
          <ModalBody>
            {this.state.modal.text}
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={()=>this.toggleModal("","",false)}>Close</Button>{' '}
            
          </ModalFooter>
        </Modal>

      </div>
    );
  }
}

export default App;
